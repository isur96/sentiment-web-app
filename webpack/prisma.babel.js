import path from "path";
import nodeExternals from "webpack-node-externals";
import CopyWebpackPlugin from "copy-webpack-plugin";

export default {
  target: "async-node",
  mode: "production",
  entry: [
    "./prisma/seed.ts",
  ],
  output: {
    filename: "seed.js",
    path: path.resolve(__dirname, "../build/prisma"),
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: "./prisma/migrations", to: "migrations/" },
        { from: "./prisma/schema.prisma", to: "schema.prisma" }
      ]
    })
  ],
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: "ts-loader",
        options: {
          transpileOnly: true,
        }
      },
    ]
  },
  resolve: {
    extensions: [".ts"],
  }
}
