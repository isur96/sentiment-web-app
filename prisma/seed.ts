import { getUsersSeed, permissions, roles } from "./seeds";
import { Permission, PrismaClient, Role, User } from "@prisma/client";

const prisma = new PrismaClient();

const main = async () => {
  const users = await getUsersSeed();
  const createdUsers: User[] = [];
  const createdRoles: Role[] = [];
  const createdPermissions: Permission[] = [];

  for(const user of users) {
    const u = await prisma.user.upsert({
      where: {
        email: user.email,
      },
      update: {},
      create: user,
    });
    createdUsers.push(u);
  }

  for(const role of roles) {
    const r = await prisma.role.upsert({
      where: { name: role.name },
      update: { },
      create: role,
    });
    createdRoles.push(r);
  }

  for(const permission of permissions) {
    const p = await prisma.permission.upsert({
      where: {
        action_subject: {
          action: permission.action,
          subject: permission.subject,
        },
       },
       create: permission,
       update: {},
    });
    createdPermissions.push(p);
  }

  const admin = createdUsers.find(user => user.username === "Admin");
  const moderator = createdUsers.find(user => user.username === "Moderator");
  const user = createdUsers.find(user => user.username === "User");
  const adminRole = createdRoles.find(role => role.name === "ADMIN");
  const moderatorRole = createdRoles.find(role => role.name === "MODERATOR");
  const userRole = createdRoles.find(role => role.name === "USER");
  await prisma.userRoles.upsert({
    where: {
      userId_roleId: {
        roleId: adminRole.id,
        userId: admin.id,
      },
     },
    create: {
      roleId: adminRole.id,
      userId: admin.id,
    },
    update: { }
  });
  await prisma.userRoles.upsert({
    where: {
      userId_roleId: {
        roleId: moderatorRole.id,
        userId: moderator.id,
      },
     },
    create: {
      roleId: moderatorRole.id,
      userId: moderator.id,
    },
    update: { }
  });
  await prisma.userRoles.upsert({
    where: {
      userId_roleId: {
        roleId: userRole.id,
        userId: user.id,
      },
     },
    create: {
      roleId: userRole.id,
      userId: user.id,
    },
    update: { }
  });
  for(const perm of createdPermissions) {
    await prisma.rolePermissions.upsert({
      where: {
        permissionId_roleId: {
          permissionId: perm.id,
          roleId: adminRole.id,
        }
      },
      create: {
        roleId: adminRole.id,
        permissionId: perm.id,
        type: "ALL",
      },
      update: {},
    })
  }
}

main().catch(err => {
  console.error(err);
  process.exit(1);
}).finally(async () => {
  await prisma.$disconnect();
});
