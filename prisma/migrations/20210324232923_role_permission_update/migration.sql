/*
  Warnings:

  - You are about to drop the column `roleId` on the `permissions` table. All the data in the column will be lost.
  - The migration will add a unique constraint covering the columns `[action,subject]` on the table `permissions`. If there are existing duplicate values, the migration will fail.

*/
-- CreateEnum
CREATE TYPE "PermissionType" AS ENUM ('SELF', 'GROUP', 'ALL');

-- DropForeignKey
ALTER TABLE "permissions" DROP CONSTRAINT "permissions_roleId_fkey";

-- AlterTable
ALTER TABLE "permissions" DROP COLUMN "roleId";

-- CreateTable
CREATE TABLE "role_permissions" (
    "permissionId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,
    "type" "PermissionType" NOT NULL DEFAULT E'SELF',

    PRIMARY KEY ("permissionId","roleId")
);

-- CreateIndex
CREATE UNIQUE INDEX "permissions.action_subject_unique" ON "permissions"("action", "subject");

-- AddForeignKey
ALTER TABLE "role_permissions" ADD FOREIGN KEY ("permissionId") REFERENCES "permissions"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "role_permissions" ADD FOREIGN KEY ("roleId") REFERENCES "roles"("id") ON DELETE CASCADE ON UPDATE CASCADE;
