/*
  Warnings:

  - A unique constraint covering the columns `[sentimentId]` on the table `reports` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "reports.sentimentId_unique" ON "reports"("sentimentId");
