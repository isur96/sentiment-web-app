import { PERMISSION } from "../../src/Shared/Constants"
interface Permission {
  action: string,
  subject: string,
}

export const permissions: Permission[] = Object.keys(PERMISSION.SUBJECTS).reduce((prev, curr) => {
  Object.keys(PERMISSION.ACTIONS).forEach(act => {
    prev.push({
      action: PERMISSION.ACTIONS[act as keyof typeof PERMISSION.ACTIONS],
      subject: PERMISSION.SUBJECTS[curr as keyof typeof PERMISSION.SUBJECTS],
    });
  });
  return prev;
}, []);
