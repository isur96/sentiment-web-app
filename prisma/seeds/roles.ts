interface Role {
  name: string,
}

export const roles: Role[] = [
  { name: "ADMIN" },
  { name: "MODERATOR" },
  { name: "USER" },
]
