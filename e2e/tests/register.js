/// <reference types="cypress" />

import { ADDRESS } from "../config";

const NAME = Math.random().toString().substr(2, 10);
const EMAIL = NAME + "@mail.com";
const PASSWORD = "password";

context("Register", () => {
  it("Registers and logs in", () => {
    cy.visit(ADDRESS + "/en/register");

    cy.get("[placeholder='Enter your email']")
      .type(EMAIL)
      .get("[placeholder='Enter your username']")
      .type(NAME)
      .get("[placeholder='Enter your password']")
      .type(PASSWORD)
      .get("[placeholder='Confirm your password']")
      .type(PASSWORD);

    cy.get("button").contains("Register").click();

    cy.contains("Register success, you may now log in to your account").should(
      "exist"
    );

    cy.get("[placeholder='Enter your login']")
      .type(NAME)
      .get("[placeholder='Enter your password']")
      .type(PASSWORD)
      .get("[type='submit']")
      .click();

    cy.wait(1000);

    cy.get("[placeholder='Enter your sentence...'").should("exist");
  });
});
