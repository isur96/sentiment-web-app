/// <reference types="cypress" />

import { ADDRESS } from "../config";

const MODERATOR_TEST_LOGIN = "Moderator";
const MODERATOR_TEST_PASSWORD = "moderator";

context("Moderator", () => {
  it("Logs in", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type(MODERATOR_TEST_LOGIN)
      .get("[placeholder='Enter your password']")
      .type(MODERATOR_TEST_PASSWORD)
      .get("[type='submit']")
      .click();

    cy.wait(100);
    cy.contains('Profile').click();

    cy.contains('Profile Data').should('exist');
  });
});
