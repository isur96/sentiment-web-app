/// <reference types="cypress" />

import { ADDRESS } from "../config";

it("Rejects invalid login data", () => {
  cy.visit(ADDRESS)
    .get("[placeholder='Enter your login']")
    .type("tets")
    .get("[placeholder='Enter your password']")
    .type("tets")
    .get("[type='submit']")
    .click()
    .get("[role=status]")
    .contains("Invalid Credentials")
    .should("exist");
});
