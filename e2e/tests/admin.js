/// <reference types="cypress" />

import { ADDRESS } from "../config";

const ADMIN_TEST_LOGIN = "Admin";
const ADMIN_TEST_PASSWORD = "admin";
const TEXT = 'faleena from el paso ' + Math.random().toString().substr(2, 10);

Cypress.on('uncaught:exception', (err, runnable, promise) => {
  // when the exception originated from an unhandled promise
  // rejection, the promise is provided as a third argument
  // you can turn off failing the test in this case
  if (promise) {
    return false
  }
  // we still want to ensure there are no other unexpected
  // errors, so we let them fail the test
})

context("Admin", () => {
  it("Rejects invalid login data", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type("tets")
      .get("[placeholder='Enter your password']")
      .type("tets")
      .get("[type='submit']")
      .click()
      .get("[role=status]")
      .contains("Invalid Credentials")
      .should("exist");
  });

  it("Logs in and runs prediction", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type(ADMIN_TEST_LOGIN)
      .get("[placeholder='Enter your password']")
      .type(ADMIN_TEST_PASSWORD)
      .get("[type='submit']")
      .click()
      .get("[placeholder='Enter your sentence...'")
      .should("exist")
      .type(TEXT)
      .root()
      .contains("Check")
      .click()
      .get("[role=status]")
      .should("exist");
  });

  it("History", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type(ADMIN_TEST_LOGIN)
      .get("[placeholder='Enter your password']")
      .type(ADMIN_TEST_PASSWORD)
      .get("[type='submit']")
      .click();

    cy.wait(100);
    cy.contains("History").should("exist").click();

    cy.contains("ALL").click();
    cy.wait(1000);
    cy.contains("MY").click();
    cy.wait(1000);

    cy.contains(TEXT).should("exist");
  });

  it("Permissions", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type(ADMIN_TEST_LOGIN)
      .get("[placeholder='Enter your password']")
      .type(ADMIN_TEST_PASSWORD)
      .get("[type='submit']")
      .click();

    cy.wait(2000);
    cy.contains("Permissions").should("exist").click();

    cy.contains('admin@admin.admin').should("exist");
  });

  it("Statistics", () => {
    cy.visit(ADDRESS)
      .get("[placeholder='Enter your login']")
      .type(ADMIN_TEST_LOGIN)
      .get("[placeholder='Enter your password']")
      .type(ADMIN_TEST_PASSWORD)
      .get("[type='submit']")
      .click();

    cy.wait(100);
    cy.contains("Statistics").should("exist").click();

    cy.contains('Self').click();
    cy.wait(100);

    cy.contains('Sentiments By Sentiment').should("exist");
    cy.contains('Sentiments By Month').should("exist");
    cy.contains('Sentiments By Sentiment').should("exist");
  });
});
