import { PermissionType } from ".prisma/client";
import { RoleObject } from "@shared/Interfaces/role.interface";

export interface CreateRoleRequestDto {
  name: string,
}

export interface CreateRoleResponseDto extends RoleObject {}

export interface UpdateRoleRequestDto {
  name: string,
}

export interface UpdateRoleResponseDto extends RoleObject {}

export interface AssignPermissionRequestDto {
  permission: string,
  type: PermissionType,
}

export interface AssignPermissionResponseDto extends RoleObject {}

export interface UnAssignPermissionRequestDto {
  permission: string,
}

export interface UnAssignPermissionResponseDto extends RoleObject {}

export type GetRolesResponseDto = RoleObject[]
