import { PermissionObject } from "@shared/Interfaces/permission.interface";

export interface CreatePermissionRequestDto {
  subject: string,
  action: string,
}

export interface CreatePermissionResponseDto extends PermissionObject { }

export type GetPermissionResponesDto = PermissionObject[];
