import { UserObject } from "../Interfaces/user.interface";

export interface MeResponseDto extends UserObject { }

export type GetUsersDtoResponse = UserObject[];
export interface AssignRoleRequestDto {
  role: string,
}

export interface UnAssignRoleRequestDto {
  role: string,
}

export interface AssignRoleResponseDto extends UserObject { }

export interface UnAssignRoleResponseDto extends UserObject { }

export interface UpdateuserRequestDto {
  username?: string,
  email?: string,
}

export interface UpdateUserResponseDto extends UserObject { }
