import { SentimentObject, StatsByMonth, StatsBySentiment } from "@shared/Interfaces/sentiment.interface";

export interface SentimentAnalyzeRequestDto {
  text: string,
}

export interface SentimentAnalyzeResponseDto extends SentimentObject { }
export type SentimentGetResponseDto = SentimentObject[];

export interface StatisticsResponseDto {
  all: number,
  bySentiment: StatsBySentiment,
  byMonths: StatsByMonth,
}
