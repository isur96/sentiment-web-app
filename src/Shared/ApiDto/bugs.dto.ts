import { BugReportObject, SentimentReportObject } from "@shared/Interfaces/bugReport.interface";

export type GetReportResponseDto = Partial<BugReportObject & SentimentReportObject>[];

export interface ReportBugRequestDto {
  text: string,
}

export interface ReportSentimentRequetsDto {
  sentimentId: number,
}

export interface BugSolveRequestDto {
  id: string,
  comment?: string,
  accept: boolean,
}

export interface SentimentSolveRequetsDto {
  id: string,
  comment?: string,
  accept: boolean,
}
