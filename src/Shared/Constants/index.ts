export { routes as API } from "./BackendRoutes";
export { paths as PATHS } from "./FrontRoutes";
export * as TIME from "./time";
export * as PERMISSION from "./Permissions";
export * as PAGINATION from "./Pagination";
