export const routes = {
  AUTH: "auth",
  USERS: "users",
  SETTINGS: "settings",
  SENTIMENT: "sentiment",
  PERMISSIONS: "permissions",
  ROLES: "roles",
  REPORTS: "reports",
};
