export const paths = {
  HOMEPAGE: "",
  LOGIN: "login",
  REGISTER: "register",
  PERMISSIONS: "permissions",
  HISTORY: "history",
  PROFILE: "profile",
  STATISTICS: "statistics",
  BUGS: "bugs",
};
