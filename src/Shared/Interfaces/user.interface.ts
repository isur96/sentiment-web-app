import { UserPermission } from "./permission.interface";
import { UserRole } from "./role.interface";

export interface UserObject {
  id: string,
  username: string,
  email: string,
  createdDate: Date | String,
  updateDate: Date | String,
  roles?: UserRole[],
  permissions?: UserPermission[],
}
