import { BugReport, Report } from ".prisma/client";

export interface BugReportObject extends BugReport {}
export interface SentimentReportObject extends Report {
  text: string,
  sentiment: number,
}
