import { PERMISSION } from "@shared/Constants";

export interface PermissionObject {
  id: string,
  subject: string,
  action: string,
  type?: PermissionType,
}

export interface UserPermission {
  subject: string,
  action: string,
  type: PermissionType,
}

export type PermissionType = keyof typeof PERMISSION.TYPES;
export type PermissionActions = keyof typeof PERMISSION.ACTIONS;
export type PermissionSubjects = keyof typeof PERMISSION.SUBJECTS;
