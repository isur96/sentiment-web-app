export interface SentimentObject {
  id: number,
  text: string,
  sentiment: number,
  date: Date,
}

export interface StatsBySentiment {
  negative: number,
  neutral: number,
  positive: number,
}

export interface StatsByMonth {
  [key: string]: number,
}
