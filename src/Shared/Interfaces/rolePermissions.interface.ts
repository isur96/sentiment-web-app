import { PermissionType } from ".prisma/client";

export interface RolePermissionObject {
  permissionId: string,
  roleId: string,
  type: PermissionType,
}
