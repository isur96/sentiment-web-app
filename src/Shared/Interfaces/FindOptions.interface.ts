export interface DefaultFindOptions<T> {
  page?: Paging,
  search?: Search<T>,
}

export interface DefaultFindOneOptions<T> {
  search: Search<T>,
}

interface Paging {
  page: number,
  pageSize: number,
}

interface Search<T> {
  search: string,
  field?: keyof T,
  fields?: (keyof T)[],
}
