import { RolePermissionObject } from "./rolePermissions.interface";
import { UserObject } from "./user.interface";

export interface RoleObject {
  id: string,
  name: string,
  permissions?: RolePermissionObject[],
  users?: UserObject[],
}

export type UserRole = string;
