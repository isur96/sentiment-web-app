import { permissionLevelCompare } from "./PermissionHelper";

describe("Permission Helper", () => {
  it("Should correctly check permissions - SELF", () => {
    expect(permissionLevelCompare("SELF", "SELF")).toBe(true);
    expect(permissionLevelCompare("GROUP", "SELF")).toBe(true);
    expect(permissionLevelCompare("ALL", "SELF")).toBe(true);
  });

  it("Should correctly check permissions - GROUP", () => {
    expect(permissionLevelCompare("SELF", "GROUP")).toBe(false);
    expect(permissionLevelCompare("GROUP", "GROUP")).toBe(true);
    expect(permissionLevelCompare("ALL", "GROUP")).toBe(true);
  });

  it("Should correctly check permissions - ALL", () => {
    expect(permissionLevelCompare("SELF", "ALL")).toBe(false);
    expect(permissionLevelCompare("GROUP", "ALL")).toBe(false);
    expect(permissionLevelCompare("ALL", "ALL")).toBe(true);
  });
});
