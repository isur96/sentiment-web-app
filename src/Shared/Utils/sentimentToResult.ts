export const sentimentToResult = (num: number): "POSITIVE" | "NEGATIVE" | "NEUTRAL" => {
  if(num < 0.3) return "NEGATIVE";
  if(num < 0.7) return "NEUTRAL";
  return "POSITIVE";
};
