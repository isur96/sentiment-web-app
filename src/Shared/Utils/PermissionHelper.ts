import { PermissionType } from "@shared/Interfaces/permission.interface";

export const permissionLevelCompare = (userType: PermissionType, requiredType: PermissionType): boolean => {
  const PermissionLevelHelper = {
    "ALL": 3,
    "GROUP": 2,
    "SELF": 1,
  };

  return PermissionLevelHelper[userType] >= PermissionLevelHelper[requiredType];
};
