import { sentimentToResult } from "./sentimentToResult";

describe("Sentiment numeric to result", () => {
  it("Should correclty translate number to result", () => {
    expect(sentimentToResult(0)).toBe("NEGATIVE");
    expect(sentimentToResult(0.5)).toBe("NEUTRAL");
    expect(sentimentToResult(1)).toBe("POSITIVE");
  });

  it("Boundary test", () => {
    expect(sentimentToResult(0)).toBe("NEGATIVE");
    expect(sentimentToResult(0.29999999)).toBe("NEGATIVE");
    expect(sentimentToResult(0.3)).toBe("NEUTRAL");
    expect(sentimentToResult(0.69999999)).toBe("NEUTRAL");
    expect(sentimentToResult(0.7)).toBe("POSITIVE");
    expect(sentimentToResult(1)).toBe("POSITIVE");
  });
});

