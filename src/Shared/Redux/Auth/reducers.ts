import {
  AuthActionTypes,
  AuthState,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
} from "./types";

const initialState: AuthState = {
  userid: null,
  loginError: null,
  registerSuccess: false,
  registerError: null,
};

export const authReducer = (state = initialState, action: AuthActionTypes): AuthState => {
  switch(action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        registerSuccess: false,
        ...action.payload,
      };
    case LOGOUT:
      return {
        ...state,
        userid: null,
        registerSuccess: false,
        loginError: null,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        userid: null,
        ...action.payload,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        registerError: null,
        registerSuccess: true,
      };
    case REGISTER_FAILED:
      return {
        ...state,
        registerSuccess: false,
        ...action.payload,
      };
    default:
      return state;
  }
};
