import { Dispatch } from "redux";
import { Cookies } from "react-cookie";
import { getLocalUser } from "../LocalUser";
import { removeLocalUser } from "../LocalUser/actions";
import { AuthActionTypes, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT, REGISTER_FAILED, REGISTER_SUCCESS } from "./types";
import { AuthService } from "@client/Services";
import { RegisterRequestDto } from "@shared/ApiDto/auth.dto";

export const login = (login: string, password: string) => async (dispatch: Dispatch<AuthActionTypes>) => {
  try {
    const { userId, access_token } = await AuthService.login({ login, password });
    new Cookies().set("access_token", access_token);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: {
        userid: userId,
      },
    });
    dispatch(getLocalUser() as any); // eslint-disable-line @typescript-eslint/no-explicit-any
  } catch(e) {
    dispatch({
      type: LOGIN_FAILED,
      payload: {
        loginError: "failed",
      },
    });
  }
};

export const logout = () => async (dispatch: Dispatch<AuthActionTypes>) => {
  await AuthService.logout();
  new Cookies().remove("access_token");
  dispatch({
    type: LOGOUT,
  });
  dispatch(removeLocalUser() as any); // eslint-disable-line @typescript-eslint/no-explicit-any
};

export const register = (data: RegisterRequestDto) => async (dispatch: Dispatch<AuthActionTypes>) => {
  try {
    await AuthService.register(data);
    dispatch({
      type: REGISTER_SUCCESS,
    });
  } catch(e) {
    dispatch({
      type: REGISTER_FAILED,
      payload: {
        registerError: "failed",
      },
    });
  }
};
