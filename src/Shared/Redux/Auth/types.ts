export interface AuthState {
  userid: string,
  loginError: string,
  registerError: string,
  registerSuccess: boolean,
}

export const LOGIN_SUCCESS = `[AUTH] LOGIN SUCCESS`;
export const LOGIN_FAILED = `[AUTH] LOGIN FAILED`;
export const LOGOUT = `[AUTH] LOGOUT`;
export const REGISTER_SUCCESS = `[AUTH] REGISTER SUCCESS`;
export const REGISTER_FAILED = `[AUTH] REGISTER FAILED`;

interface ActionLoginSuccess {
  type: typeof LOGIN_SUCCESS,
  payload: {
    userid: string,
  },
}

interface ActionLoginFailed {
  type: typeof LOGIN_FAILED,
  payload: {
    loginError: string,
  },
}

interface ActionLogout {
  type: typeof LOGOUT,
}

interface ActionRegisterSuccess {
  type: typeof REGISTER_SUCCESS,
}

interface ActionRegisterFailed {
  type: typeof REGISTER_FAILED,
  payload: {
    registerError: string,
  },
}

export type AuthActionTypes = ActionLoginSuccess | ActionLogout | ActionLoginFailed | ActionRegisterSuccess | ActionRegisterFailed;
