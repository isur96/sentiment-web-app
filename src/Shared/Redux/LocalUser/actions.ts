import { Dispatch } from "redux";
import { GET_LOCALUSER, StateLocalUser, LocalUserActionTypes, REMOVE_LOCALUSER, UPDATE_USER } from "./types";
import { UsersService } from "@client/Services";
import { UpdateuserRequestDto } from "@shared/ApiDto/users.dto";

export const getLocalUser = () => async (dispatch: Dispatch<LocalUserActionTypes>) => {
  const me = await UsersService.getMe();
  dispatch({
    type: GET_LOCALUSER,
    payload: me,
  });
};

export const updateLocalUser = (data: UpdateuserRequestDto) => async (dispatch: Dispatch<LocalUserActionTypes>) => {
  const me = await UsersService.updateUser(data);
  dispatch({
    type: UPDATE_USER,
    payload: me,
  });
};

export const removeLocalUser = () => async (dispatch: Dispatch<LocalUserActionTypes>) => {
  dispatch({ type: REMOVE_LOCALUSER });
};
