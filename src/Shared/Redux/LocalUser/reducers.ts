import { StateLocalUser, LocalUserActionTypes, GET_LOCALUSER, REMOVE_LOCALUSER, UPDATE_USER } from "./types";

const initialState: StateLocalUser = {
  createdDate: null,
  email: null,
  id: null,
  updateDate: null,
  username: null,
  permissions: [],
  roles: [],
};

export const localUserReducer = (state = initialState, action: LocalUserActionTypes): StateLocalUser => {
  switch(action.type) {
    case GET_LOCALUSER:
      return {
        ...state,
        ...action.payload,
      };
    case REMOVE_LOCALUSER:
      return {
        ...initialState,
      };
    case UPDATE_USER:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
