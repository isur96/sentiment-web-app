import { UserObject } from "@shared/Interfaces/user.interface";

export interface StateLocalUser extends UserObject { }

export const GET_LOCALUSER = `[LOCALUSER] GET`;
export const REMOVE_LOCALUSER = `[LOCALUSER] REMOVE`;
export const UPDATE_USER = `[LOCALUSER] UPDATE`;
interface ActionGetLocalUser {
  type: typeof GET_LOCALUSER,
  payload: UserObject,
}

interface ActionRemoveLocalUser {
  type: typeof REMOVE_LOCALUSER,
}

interface UpdateLocalUser {
  type: typeof UPDATE_USER,
  payload: UserObject,
}

export type LocalUserActionTypes = ActionGetLocalUser | ActionRemoveLocalUser | UpdateLocalUser;
