import { User } from ".prisma/client";
import { DefaultFindOneOptions, DefaultFindOptions } from "@shared/Interfaces/FindOptions.interface";

export interface FindUsersOptions extends DefaultFindOptions<User> {
  permissions?: boolean,
}

export interface FindUserOptions extends DefaultFindOneOptions<User> {
  permissions?: boolean,
}
