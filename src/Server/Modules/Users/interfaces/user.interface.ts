import { PermissionType, User as PrismaUser } from ".prisma/client";

export interface User extends PrismaUser {
  roles?: {
    userId: string,
    roleId: string,
    role: {
      id: string,
      name: string,
      permissions: {
        permissionId: string,
        roleId: string,
        type: PermissionType,
        permission: {
          id: string,
          action: string,
          subject: string,
        },
      }[],
    },
  }[],
}
