import { User } from ".prisma/client";

export interface SearchUser {
  field: keyof User,
  value: string,
}
