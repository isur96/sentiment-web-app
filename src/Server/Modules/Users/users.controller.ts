import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import BaseController from "../BaseController";
import { ApiAuthentication, ApiValidator, ApiAuthorization } from "../../Middlewares";
import UsersService from "./users.service";
import { assignRoleValidation } from "./validations";
import { getUsersValidation, unAssignRoleValidation, userUpdateValidation } from "./validations/user.validations";
import { AssignRoleRequestDto, AssignRoleResponseDto, GetUsersDtoResponse, MeResponseDto, UnAssignRoleRequestDto, UnAssignRoleResponseDto, UpdateuserRequestDto, UpdateUserResponseDto } from "@shared/ApiDto/users.dto";
import { API, PAGINATION } from "@shared/Constants";

@Service()
class UsersController extends BaseController {
  public basePath = `/${API.USERS}`;
  @Inject()
  private readonly _usersService: UsersService;

  public constructor() {
    super();
    this._initRoutes();
  }

  protected _initRoutes = () => {
    this.router.get("/me", ApiAuthentication, ApiAuthorization("USERS", "READ", "SELF"), this._getMe);
    this.router.get("/", ApiAuthentication, ApiAuthorization("USERS", "READ", "ALL"), ApiValidator(getUsersValidation), this._getUsers);
    this.router.patch("/:user/assignRole", ApiAuthentication, ApiAuthorization("USERS", "UPDATE", "ALL"), ApiValidator(assignRoleValidation), this._assignRole);
    this.router.patch("/:user/unassignRole", ApiAuthentication, ApiAuthorization("USERS", "UPDATE", "ALL"), ApiValidator(unAssignRoleValidation), this._unAssignRole);
    this.router.patch("/", ApiAuthentication, ApiAuthorization("USERS", "UPDATE", "SELF"), ApiValidator(userUpdateValidation), this._updateUser);
    this.router.patch("/:user", ApiAuthentication, ApiAuthorization("USERS", "UPDATE", "ALL"), ApiValidator(userUpdateValidation), this._updateUser);
  }

  private _getUsers = async (req: Request<{}, {}, {}, { search: string, permissions: string }>, res: Response<GetUsersDtoResponse>) => {
    const { search, permissions } = req.query;
    const users = await this._usersService.getUsers({
      page: {
        page: PAGINATION.PAGE_FIRST,
        pageSize: PAGINATION.PAGE_SIZE_ALL,
      },
      permissions: permissions === "true",
      search: {
        fields: ["email", "username"],
        search: search,
      },
    });
    res.json(users);
  }

  private _getMe = async (req: Request, res: Response<MeResponseDto>) => {
    const { userid } = req.session;
    const user = await this._usersService.findUser({ search: { field: "id", search: userid }, permissions: true });
    res.json(user);
  }

  private _assignRole = async (req: Request<{ user: string }, {}, AssignRoleRequestDto>, res: Response<AssignRoleResponseDto>) => {
    const { role } = req.body;
    const { user } = req.params;
    await this._usersService.assignRole({ role, user });
    res.json();
  }

  private _unAssignRole = async (req: Request<{ user: string }, {}, UnAssignRoleRequestDto>, res: Response<UnAssignRoleResponseDto>) => {
    const { role } = req.body;
    const { user } = req.params;
    await this._usersService.unAssignRole({ role, user });
    res.json();
  }

  private _updateUser = async (req: Request<{ user: string}, {}, UpdateuserRequestDto>, res: Response<UpdateUserResponseDto>) => {
    const { email, username } = req.body;
    const user = req.params.user ? req.params.user : req.session.userid;
    const updated = await this._usersService.updateUser(user, { email, username });
    res.json({ ...updated });
  }
}

export default UsersController;
