import { body, param, query } from "express-validator";

export const assignRoleValidation = [
  param("user").isUUID()
    .notEmpty(),
  body("role").isUUID()
    .notEmpty(),
];

export const unAssignRoleValidation = [
  param("user").isUUID()
    .notEmpty(),
  body("role").isUUID()
    .notEmpty(),
];

export const getUsersValidation = [
  query("search").optional()
    .isAlphanumeric(),
  query("permissions").optional()
    .isBoolean(),
];

export const userUpdateValidation = [
  query("user").isUUID()
    .optional(),
  body("email").isEmail()
    .optional(),
  body("username").isAlphanumeric()
    .optional(),
];
