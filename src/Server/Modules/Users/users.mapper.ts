import { Service } from "typedi";
import { User } from "./interfaces";
import { UserObject } from "@shared/Interfaces/user.interface";

@Service()
export class UsersMapper {
  public userMapper = (user: User): UserObject => {
    if(user.roles) {
      return this._userWitRolesAndPermissions(user);
    } else {
      return this._userWithoutPassword(user);
    }
  }

  private _userWitRolesAndPermissions = (user: User): UserObject => {
    const { roles, password, createdDate, updateDate, ...rest } = user;

    const { permissions, roles: rolesNames } = roles.reduce((prev, curr) => {
      prev.roles.push(curr.role.name);
      const perms = curr.role.permissions.map(perm => ({
        subject: perm.permission.subject,
        action: perm.permission.action,
        type: perm.type,
      }));
      prev.permissions.push(...perms);
      return prev;
    }, { roles: [], permissions: [] });

    return {
      ...rest,
      createdDate: createdDate.toISOString(),
      updateDate: updateDate.toISOString(),
      permissions,
      roles: rolesNames,
    };
  }

  private _userWithoutPassword = (user: User): UserObject => {
    const { password, roles, createdDate, updateDate, ...rest } = user;
    return {
      createdDate: createdDate.toISOString(),
      updateDate: updateDate.toISOString(),
      ...rest,
    };
  }
}
