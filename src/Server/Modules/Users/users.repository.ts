import { Inject, Service } from "typedi";
import { CreateUser, FindUserOptions, FindUsersOptions, UpdateUser, User } from "./interfaces";
import { Database, Logger } from "@server/Utils";

@Service()
class UsersRepository {
  @Inject()
  private readonly _db: Database;

  public createUser = async (data: CreateUser): Promise<User> => {
    const user = await this._db.client.user.create({ data });
    return user;
  }

  public updateUser = async (id: string, data: UpdateUser): Promise<User> => {
    const user = await this._db.client.user.update({ where: { id }, data });
    return user;
  }

  private _whereOptions = ({ search }: FindUsersOptions) => {
    let where = {  };
    const or: { [field: string]: { contains: string }}[] = [];
    if(search && search.search && search.fields && search.fields.length > 0) {
      search.fields.forEach(field => {
        or.push({ [field]: { contains: search.search } });
      });
      where = { OR: or };
    }
    if(search && search.search && search.field) {
      where = { [search.field]: search.search };
    }

    return where;
  }

  private _includeOptions = ({ permissions }: FindUsersOptions) => {
    if(permissions) {
      return {
        roles: {
          include: { role: {
            include: {
              permissions: {
                include: {
                  permission: true,
                },
              },
            },
          } },
        },
      };
    } else {
      return null;
    }
  }

  private _pagination = ({ page }: FindUsersOptions) => {
    if(page.pageSize === -1) return null;

    const skip = page.pageSize * (page.page - 1);
    const take = page.pageSize;
    return { skip, take };
  }

  public findUsers = async (options: FindUsersOptions): Promise<User[]> => {
    const where = this._whereOptions(options);
    const include = this._includeOptions(options);
    const pagination = this._pagination(options);
    Logger.ErrorHandler({ where, include, pagination });
    const users = await this._db.client.user.findMany({ where, include, ...pagination });
    return users;
  }

  public findUser = async (options: FindUserOptions): Promise<User> => {
    const where = this._whereOptions(options);
    const include = this._includeOptions(options);
    const user = await this._db.client.user.findFirst({
      where,
      include,
    });
    return user;
  }

  public findToLogin = async (login: string): Promise<User> => {
    const user = await this._db.client.user.findFirst({
      where: {
        OR: [{ username: login }, { email: login }],
      },
    });
    return user;
  }
}

export default UsersRepository;
