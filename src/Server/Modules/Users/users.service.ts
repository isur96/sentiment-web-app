import { Inject, Service } from "typedi";
import { BCrypt } from "../../Utils";
import { RolesRepository } from "../Roles";
import UsersRepository from "./users.repository";
import { UsersMapper } from "./users.mapper";
import { AssignRole, CreateUser, UnAssignRole, User, FindUsersOptions, FindUserOptions, UpdateUser } from "./interfaces";
import { UserObject } from "@shared/Interfaces/user.interface";

@Service()
class UsersService {
  @Inject()
  private readonly _usersRepository: UsersRepository;

  @Inject()
  private readonly _rolesRepository: RolesRepository;

  @Inject()
  private readonly _brycpt: BCrypt;

  @Inject()
  private readonly _usersMapper: UsersMapper;

  public getUsers = async (options: FindUsersOptions): Promise<UserObject[]> => {
    const users = await this._usersRepository.findUsers(options);
    return users.map(user => this._usersMapper.userMapper(user));
  }

  public createUser = async (userData: CreateUser): Promise<string> => {
    const hash = await this._brycpt.hashData(userData.password);
    const u = await this._usersRepository.createUser({
      ...userData, password: hash,
    });

    return u.id;
  }

  public findUser = async (options: FindUserOptions): Promise<UserObject> => {
    const user = await this._usersRepository.findUser(options);

    if(!user) return null;

    return this._usersMapper.userMapper(user);
  }

  public findUserWithPasswordByLogin = async (login: string): Promise<User> => {
    const user = await this._usersRepository.findToLogin(login);
    if(!user) return null;
    return user;
  }

  public assignRole = async (data: AssignRole) => {
    return await this._rolesRepository.assignUser(data);
  }

  public unAssignRole = async (data: UnAssignRole) => {
    return await this._rolesRepository.deleteUser(data);
  }

  public updateUser = async (id: string, data: UpdateUser): Promise<UserObject> => {
    const user = await this._usersRepository.updateUser(id, data);
    return this._usersMapper.userMapper(user);
  }
}

export default UsersService;
