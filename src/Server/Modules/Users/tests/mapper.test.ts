import { UsersMapper } from "../users.mapper";
import { correctlyMapped, userTest, correctlyMappedWithPermissions, userTestPermissions } from "./userTestData";

describe("User Mapper Test", () => {
  const mapper = new UsersMapper();

  it("Should correctly map user data with permissions", () => {
    const mapped = mapper.userMapper(userTestPermissions);
    expect(mapped).toMatchObject(correctlyMappedWithPermissions);
  });

  it("Should correclty map user data", () => {
    const mapped = mapper.userMapper(userTest);
    expect(mapped).toMatchObject(correctlyMapped);
  });
});
