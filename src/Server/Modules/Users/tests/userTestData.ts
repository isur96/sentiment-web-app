import { User } from "../interfaces/user.interface";
import { UserObject } from "@shared/Interfaces/user.interface";

export const userTest: User = {
  id: "71bc3da0-d623-4472-888f-a4affef7fff5",
  username: "Admin",
  email: "admin@admin.admin",
  createdDate: new Date("2021-03-24T20:04:55.008Z"),
  updateDate: new Date("2021-03-24T20:04:55.008Z"),
  password: "$2a$10$ENooTXzsB0aRf7PP945FEOC3ifjJumIBdkByi9.DQXROTM2XRl7ie",
};

export const userTestPermissions: User = {
  ...userTest,
  roles: [
    {
      userId: "71bc3da0-d623-4472-888f-a4affef7fff5",
      roleId: "9aa9a0bd-f890-4032-a854-437b7cde57f0",
      role: {
        id: "9aa9a0bd-f890-4032-a854-437b7cde57f0",
        name: "ADMIN",
        permissions: [
          {
            permissionId: "5c5aaee7-c021-48f9-aa74-5ad69321ac44",
            roleId: "9aa9a0bd-f890-4032-a854-437b7cde57f0",
            type: "ALL",
            permission: {
              id: "5c5aaee7-c021-48f9-aa74-5ad69321ac44",
              action: "add",
              subject: "permission",
            },
          },
          {
            permissionId: "547f33a5-f6f5-46e4-a6fc-5bf506b47831",
            roleId: "9aa9a0bd-f890-4032-a854-437b7cde57f0",
            type: "ALL",
            permission: {
              id: "547f33a5-f6f5-46e4-a6fc-5bf506b47831",
              action: "delete",
              subject: "permission",
            },
          },
          {
            permissionId: "05f55c7a-a103-4b8c-bbcf-8003375bc3f0",
            roleId: "9aa9a0bd-f890-4032-a854-437b7cde57f0",
            type: "ALL",
            permission: {
              id: "05f55c7a-a103-4b8c-bbcf-8003375bc3f0",
              action: "update",
              subject: "permission",
            },
          },
        ],
      },
    },
  ],
};

export const correctlyMapped: UserObject = {
  id: "71bc3da0-d623-4472-888f-a4affef7fff5",
  username: "Admin",
  email: "admin@admin.admin",
  createdDate: "2021-03-24T20:04:55.008Z",
  updateDate: "2021-03-24T20:04:55.008Z",
};

export const correctlyMappedWithPermissions: UserObject = {
  ...correctlyMapped,
  roles: ["ADMIN"],
  permissions: [
    { action: "add", subject: "permission", type: "ALL" },
    { action: "delete", subject: "permission", type: "ALL" },
    { action: "update", subject: "permission", type: "ALL" },
  ],
};
