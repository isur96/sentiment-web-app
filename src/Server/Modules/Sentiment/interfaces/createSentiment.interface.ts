export interface CreateSentiment {
  text: string,
  sentiment: number,
  userId: string,
  processed: string[],
}
