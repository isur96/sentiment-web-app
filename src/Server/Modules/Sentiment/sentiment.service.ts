import { Inject, Service } from "typedi";
import { Sentiment } from "@prisma/client";
import { SentimentClient } from "@server/Clients";
import { SentimentObject } from "@shared/Interfaces/sentiment.interface";
import { SentimentRepository } from "@server/Modules/Sentiment/sentiment.repository";
import { DefaultFindOptions } from "@shared/Interfaces/FindOptions.interface";
import { StatisticsResponseDto } from "@shared/ApiDto/sentiment.dto";

@Service()
export class SentimentService {
  @Inject()
  private readonly _sentimentClient: SentimentClient;

  @Inject()
  private readonly  _sentimentRepository: SentimentRepository;

  public analyze = async (text: string, userId: string): Promise<SentimentObject> => {
    const sent = await this._sentimentClient.request({ text });
    const sentiment = await this._sentimentRepository.createSentiment({ ...sent, userId });
    return sentiment;
  }

  public get = async (options: DefaultFindOptions<Sentiment>): Promise<SentimentObject[]> => {
    const sentiments = await this._sentimentRepository.getSentiments(options);
    return sentiments;
  }

  public statistics = async (user?: string): Promise<StatisticsResponseDto> => {
    const stats = await this._sentimentRepository.getStatistics(user);
    return {
      bySentiment: {
        negative: stats.negative,
        positive: stats.positive,
        neutral: stats.neutral,
      },
      byMonths: stats.byMonths.reduce((prev: {[key: string]: number}, curr: { year: number, month: number, count: number }) => {
        prev[`${curr.year}/${curr.month}`] = curr.count;
        return prev;
      }, {}),
      all: stats.all.count,
    };
  }
}
