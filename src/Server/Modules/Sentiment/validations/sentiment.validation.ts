import { body, param, query } from "express-validator";

export const sentimentValidation = [
  body("text").isString()
    .notEmpty(),
];

export const getValidation = [
  query("page").isNumeric()
    .optional(),
  query("pageSize").isNumeric()
    .optional(),
];

export const getStatisticsValitadion = [
  param("user").isUUID()
    .optional(),
];
