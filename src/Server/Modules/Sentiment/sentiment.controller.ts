import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import BaseController from "@server/Modules/BaseController";
import { API, PAGINATION } from "@shared/Constants";
import { ApiAuthentication, ApiAuthorization, ApiValidator } from "@server/Middlewares";
import { SentimentAnalyzeRequestDto, SentimentAnalyzeResponseDto, SentimentGetResponseDto, StatisticsResponseDto } from "@shared/ApiDto/sentiment.dto";
import { SentimentService } from "@server/Modules/Sentiment/sentiment.service";
import { getStatisticsValitadion, getValidation, sentimentValidation } from "@server/Modules/Sentiment/validations";

@Service()
export class SentimentController extends BaseController {
  public basePath = `/${API.SENTIMENT}`;

  @Inject()
  private readonly _sentimentService: SentimentService;

  public constructor() {
    super();
    this._initRoutes();
  }

  protected _initRoutes = () => {
    this.router.post("/", ApiAuthentication, ApiValidator(sentimentValidation), this._analyze);
    this.router.get("/", ApiAuthentication, ApiAuthorization("SENTIMENTS", "READ", "ALL"), ApiValidator(getValidation), this._get(true));
    this.router.get("/my", ApiAuthentication, ApiAuthorization("SENTIMENTS", "READ", "SELF"), ApiValidator(getValidation), this._get(false));
    this.router.get("/statistics/:user?", ApiAuthentication, ApiAuthorization("STATISTICS", "READ", "SELF"), ApiValidator(getStatisticsValitadion), this._statistics);
  };

  private _analyze = async (req: Request<{}, {}, SentimentAnalyzeRequestDto>, res: Response<SentimentAnalyzeResponseDto>) => {
    const analyze = await this._sentimentService.analyze(req.body.text, req.session.userid);
    res.json({ ...analyze });
  }

  private _get = (all: boolean) => async (req: Request<{}, {}, {}, { page: string, pageSize: string }>, res: Response<SentimentGetResponseDto>) => {
    const { page, pageSize } = req.query;
    const { userid } = req.session;
    const pageInt = page ? parseInt(page) : PAGINATION.PAGE_FIRST;
    const pageSizeInt = pageSize ? parseInt(pageSize) : PAGINATION.PAGE_SIZE;
    const sentiments = await this._sentimentService.get({
      page: {
        page: pageInt,
        pageSize: pageSizeInt,
      },
      ...all ? null : { search: { search: userid, field: "userId" } },
    });
    res.json(sentiments);
  }

  private _statistics = async (req: Request, res: Response<StatisticsResponseDto>) => {
    const { user } = req.params;
    const statistics = await this._sentimentService.statistics(user);
    res.json(statistics);
  }
}
