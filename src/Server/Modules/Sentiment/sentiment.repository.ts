import { Inject, Service } from "typedi";
import { Sentiment } from "@prisma/client";
import { Database } from "@server/Utils";
import { CreateSentiment } from "@server/Modules/Sentiment/interfaces";
import { DefaultFindOptions } from "@shared/Interfaces/FindOptions.interface";

@Service()
export class SentimentRepository {
  @Inject()
  private readonly _db: Database;

  private _whereOptions = ({ search }: DefaultFindOptions<Sentiment>) => {
    let where = {  };
    const or: { [field: string]: { contains: string }}[] = [];
    if(search && search.search && search.fields && search.fields.length > 0) {
      search.fields.forEach(field => {
        or.push({ [field]: { contains: search.search } });
      });
      where = { OR: or };
    }
    if(search && search.search && search.field) {
      where = { [search.field]: search.search };
    }
    return where;
  }

  private _pagination = ({ page }: DefaultFindOptions<Sentiment>) => {
    if(page.pageSize === -1) return null;

    const skip = page.pageSize * (page.page - 1);
    const take = page.pageSize;
    return { skip, take };
  }

  public createSentiment = async (data: CreateSentiment): Promise<Sentiment> => {
    const sentiment = await this._db.client.sentiment.create({ data });
    return sentiment;
  }

  public getSentiments = async (options: DefaultFindOptions<Sentiment>): Promise<Sentiment[]> => {
    const where = this._whereOptions(options);
    const pagination = this._pagination(options);
    const sentiments = await this._db.client.sentiment.findMany({
      where,
      ...pagination,
      orderBy: {
        date: "desc",
      },
    });
    return sentiments;
  }

  public getStatistics = async (userid: string) => {
    let where = "";
    if(userid) where = `WHERE "userId" = '${userid}'`;
    const result = await this._db.client.$queryRaw(`
    SELECT
      CASE
          WHEN sentiment < 0.3 THEN 'negative'
          WHEN sentiment > 0.7 THEN 'positive'
          ELSE 'neutral'
      END as "sentiment",
      COUNT(*)
    FROM sentiments
    ${where}
    GROUP BY
      CASE
          WHEN sentiment < 0.3 THEN 'negative'
          WHEN sentiment > 0.7 THEN 'positive'
          ELSE 'neutral'
      END;
    `);

    const byMonths = await this._db.client.$queryRaw(`
      SELECT
        COUNT(*),
        EXTRACT(MONTH from date) as "month",
        EXTRACT(YEAR from date) as "year"
      FROM sentiments
      ${where}
      GROUP BY EXTRACT(MONTH from date), EXTRACT(YEAR from date)
      ORDER BY year ASC, month ASC;
    `);

    const all = await this._db.client.$queryRaw(`SELECT COUNT(*) FROM sentiments ${where};`);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const positive = result.find((item: any) => item.sentiment == "positive");
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const negative = result.find((item: any) => item.sentiment == "negative");
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const neutral = result.find((item: any) => item.sentiment == "neutral");
    const bySentiment = {
      positive: positive ? positive.count : 0,
      negative: negative ? negative.count : 0,
      neutral: neutral ? neutral.count : 0,
    };
    return { ...bySentiment, byMonths, all: all[0] };
  }
}
