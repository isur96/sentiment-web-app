export * from "./assignPermission.interface";
export * from "./createRole.interface";
export * from "./deletePermission.interface";
export * from  "./deleteRole.interface";
export * from "./updateRole.interface";
export * from  "./where.interface";
