export interface DeletePermission {
  id: string,
  permission: string,
}
