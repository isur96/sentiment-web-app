import { PermissionType } from ".prisma/client";

export interface AssignPermission {
  id: string,
  permission: string,
  type: PermissionType,
}
