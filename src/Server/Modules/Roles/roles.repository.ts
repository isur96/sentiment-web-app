import { Inject, Service } from "typedi";
import { AssignRole } from "../Users/interfaces/assignRole.interface";
import { AssignPermission, CreateRole, DeletePermission, UpdateRole, WhereRole } from "./interfaces";
import { Role } from ".prisma/client";
import { Database } from "@server/Utils";

@Service()
export class RolesRepository {
  @Inject()
  private readonly _db: Database;

  public get = async (): Promise<Role[]> => {
    return await this._db.client.role.findMany({ include: { permissions: true } });
  }

  public getOne = async (where: WhereRole): Promise<Role> => {
    return await this._db.client.role.findFirst({ where });
  };

  public create = async (data: CreateRole): Promise<Role> => {
    return await this._db.client.role.create({ data });
  }

  public delete = async (id: string): Promise<void> => {
    await this._db.client.role.delete({ where: { id } });
  }

  public update = async (data: UpdateRole): Promise<Role> => {
    const { id, ...update } = data;
    const role = await this._db.client.role.update({ where: { id }, data: { ...update } });
    return role;
  }

  public assignPermission = async (data: AssignPermission) => {
    const { id, permission, type } = data;
    const role = await this._db.client.role.update({
      where: { id },
      data: {
        permissions: {
          upsert: {
            where: {
              permissionId_roleId: {
                permissionId: permission,
                roleId: id,
              },
            },
            create: {
              permissionId: permission,
              type,
            },
            update: {
              permissionId: permission,
              type,
            },
          },
        },
      },
    });
    return role;
  }

  public deletePermission = async (data: DeletePermission) => {
    const { id, permission } = data;
    const role = await this._db.client.role.update({
      where: { id },
      data: {
        permissions: {
          delete: {
            permissionId_roleId: {
              permissionId: permission,
              roleId: id,
            },
          },
        },
      },
    });
    return role;
  }

  public assignUser = async (data: AssignRole) => {
    const { role, user } = data;
    await this._db.client.userRoles.create({
      data: {
        roleId: role,
        userId: user,
      },
    });
  }

  public deleteUser = async (data: AssignRole) => {
    const { role: id, user } = data;
    const role = await this._db.client.userRoles.delete({
      where: {
        userId_roleId: {
          roleId: id,
          userId: user,
        },
      },
    });
    return role;
  }
}
