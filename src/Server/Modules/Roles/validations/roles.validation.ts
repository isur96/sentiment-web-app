import { body, param } from "express-validator";

export const createValidation = [
  body("name").isString()
    .notEmpty(),
];

export const deleteValidation = [
  param("id").isString()
    .notEmpty(),
];

export const assignPermissionValidation = [
  param("id").isUUID()
    .notEmpty(),
  body("permission").isUUID()
    .notEmpty(),
  body("type").isIn(["ALL", "GROUP", "SELF"])
    .notEmpty(),
];

export const unAssignPermissionValidation = [
  param("id").isUUID()
    .notEmpty(),
  body("permission").isUUID()
    .notEmpty(),
];

export const updateValidation = [
  param("id").isString()
    .notEmpty(),
  body("name").isString()
    .notEmpty(),
];
