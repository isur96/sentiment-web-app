import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import BaseController from "../BaseController";
import { assignPermissionValidation, createValidation, deleteValidation, updateValidation } from "./validations";
import { RolesService } from "./roles.service";
import { unAssignPermissionValidation } from "./validations/roles.validation";
import { API } from "@shared/Constants";
import { ApiAuthentication, ApiAuthorization, ApiValidator } from "@server/Middlewares";
import { AssignPermissionRequestDto, AssignPermissionResponseDto, CreateRoleRequestDto, CreateRoleResponseDto, GetRolesResponseDto, UnAssignPermissionRequestDto, UnAssignPermissionResponseDto, UpdateRoleRequestDto, UpdateRoleResponseDto } from "@shared/ApiDto/role.dto";

@Service()
export class RolesController extends BaseController {
  public basePath = `/${API.ROLES}`;

  @Inject()
  private readonly _rolesService: RolesService;

  public constructor() {
    super();
    this._initRoutes();
  }

  protected _initRoutes = () => {
    this.router.get("/",            ApiAuthentication,  ApiAuthorization("ROLES", "READ", "ALL"), this._getRoles);
    this.router.post("/",            ApiAuthentication,  ApiAuthorization("ROLES", "CREATE", "ALL"), ApiValidator(createValidation), this._createRole);
    this.router.delete("/:id",         ApiAuthentication,  ApiAuthorization("ROLES", "DELETE", "ALL"), ApiValidator(deleteValidation), this._deleteRole);
    this.router.patch("/:id/assign",  ApiAuthentication,  ApiAuthorization("ROLES", "UPDATE", "ALL"), ApiValidator(assignPermissionValidation), this._assignPermission);
    this.router.patch("/:id/unassign",  ApiAuthentication,  ApiAuthorization("ROLES", "UPDATE", "ALL"), ApiValidator(unAssignPermissionValidation), this._unAssignPermission);
    this.router.patch("/:id",         ApiAuthentication,  ApiAuthorization("ROLES", "UPDATE", "ALL"), ApiValidator(updateValidation), this._updateRole);
  }

  private _getRoles = async (req: Request, res: Response<GetRolesResponseDto>) => {
    const roles = await this._rolesService.getRoles();
    res.json(roles);
  }

  private _createRole = async (req: Request<{}, {}, CreateRoleRequestDto>, res: Response<CreateRoleResponseDto>) => {
    const role = await this._rolesService.createRole(req.body);
    res.json(role);
  }

  private _deleteRole = async (req: Request<{ id: string }, {}, {}>, res: Response) => {
    await this._rolesService.deleteRole(req.params);
    res.json();
  }

  private _assignPermission = async (req: Request<{ id: string }, {}, AssignPermissionRequestDto>, res: Response<AssignPermissionResponseDto>) => {
    const { id } = req.params;
    const { permission, type } = req.body;
    const role = await this._rolesService.assignPermission({ id, permission, type });
    res.json(role);
  }

  private _unAssignPermission = async (req: Request<{ id: string }, {}, UnAssignPermissionRequestDto>, res: Response<UnAssignPermissionResponseDto>) => {
    const { permission } = req.body;
    const { id } = req.params;
    const role = await this._rolesService.removePermission({ id, permission });
    res.json(role);
  }

  private _updateRole = async (req: Request<{id: string}, {}, UpdateRoleRequestDto>, res: Response<UpdateRoleResponseDto>) => {
    const role = await this._rolesService.updateRole({ ...req.params, ...req.body });
    res.json(role);
  }
}
