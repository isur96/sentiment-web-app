import { Inject, Service } from "typedi";
import { AssignPermission, CreateRole, DeletePermission, DeleteRole, UpdateRole } from "./interfaces";
import { RolesRepository } from "./roles.repository";
import { RoleObject } from "@shared/Interfaces/role.interface";
import { Logger } from "@server/Utils";
import HTTPError from "@server/HttpErrors";

@Service()
export class RolesService {
  @Inject()
  private readonly _roleRepository: RolesRepository;

  public getRoles = async (): Promise<RoleObject[]> => {
    return await this._roleRepository.get();
  }

  public createRole = async (data: CreateRole): Promise<RoleObject> => {
    const existingRole = await this._roleRepository.getOne(data);
    if(existingRole) throw new HTTPError.BadRequest("Role already exists!");
    return await this._roleRepository.create(data);
  }

  public deleteRole = async (data: DeleteRole): Promise<void> => {
    const existingRole = await this._roleRepository.getOne(data);
    if(!existingRole) throw new HTTPError.NotFound();
    return await this._roleRepository.delete(data.id);
  }

  public updateRole = async (data: UpdateRole): Promise<RoleObject> => {
    const role = await this._roleRepository.getOne({ id: data.id });
    if(!role) {
      throw new HTTPError.NotFound();
    }
    try {
      return await this._roleRepository.update(data);
    } catch(err) {
      throw new HTTPError.BadRequest("Role already exsits");
    }
  }

  public assignPermission = async (data: AssignPermission): Promise<RoleObject> => {
    try {
      return await this._roleRepository.assignPermission(data);
    } catch{
      throw new HTTPError.BadRequest("Not exists or already assinged");
    }
  }

  public removePermission = async (data: DeletePermission): Promise<RoleObject> => {
    try {
      return await this._roleRepository.deletePermission(data);
    } catch{
      throw new HTTPError.NotFound();
    }
  }
}
