import { body } from "express-validator";

export const reportBugValidation = [
  body("text").isString()
    .notEmpty(),
];

export const reportSentimentValidation = [
  body("sentimentId").isNumeric()
    .notEmpty(),
];

export const reportSolveBug = [
  body("id").isUUID()
    .notEmpty(),
  body("comment").isString()
    .optional(),
];

export const reportSolveSentiment = [
  body("id").isUUID()
    .notEmpty(),
  body("comment").isString()
    .optional(),
];
