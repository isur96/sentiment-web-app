import { Inject, Service } from "typedi";
import { CreateBugReport, CreateSentimentReport, ResolveBugReport, ResolveSentimentReport } from "./interfaces";
import ReportsRepository from "./reports.repository";

@Service()
class ReportsService {
  @Inject()
  private readonly _reportsRepository: ReportsRepository;

  public getReports = async (userId?: string) => {
    const reports = await this._reportsRepository.getReports(userId);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const mapped = reports.map((report: any) => {
      if(report.sentiment) {
        report.text = report.sentiment.text;
        report.sentiment = report.sentiment.sentiment;
      }
      return report;
    });
    return mapped;
  }

  public reportBug = async (data: CreateBugReport) => {
    await this._reportsRepository.createReportBug(data);
  }

  public reportSentiment = async (data: CreateSentimentReport) => {
    await this._reportsRepository.createReportSentiment(data);
  }

  public resolveBug = async (data: ResolveBugReport) => {
    await this._reportsRepository.resolveBugReport(data);
  }

  public resolveSentiment = async (data: ResolveSentimentReport) => {
    await this._reportsRepository.reslolveReportSentiment(data);
  }
}

export default ReportsService;
