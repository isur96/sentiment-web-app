import { Response, Request } from "express";
import { Inject, Service } from "typedi";
import BaseController from "../BaseController";
import ReportsService from "./reports.service";
import { reportBugValidation, reportSentimentValidation, reportSolveBug, reportSolveSentiment } from "./validations/validation";
import { API } from "@shared/Constants";
import { ApiAuthentication, ApiAuthorization, ApiValidator } from "@server/Middlewares";
import { BugSolveRequestDto, GetReportResponseDto, ReportBugRequestDto, ReportSentimentRequetsDto, SentimentSolveRequetsDto } from "@shared/ApiDto/bugs.dto";

@Service()
class ReportsController extends BaseController {
  public basePath = `/${API.REPORTS}`;
  @Inject()
  private readonly _reportsService: ReportsService;

  public constructor() {
    super();
    this._initRoutes();
  }

  protected _initRoutes = () => {
    this.router.get("/", ApiAuthentication, ApiAuthorization("BUGS", "READ", "ALL"), this._getReports);
    this.router.get("/my", ApiAuthentication, ApiAuthorization("BUGS", "READ", "SELF"), this._getMyReports);
    this.router.post("/bug", ApiAuthentication, ApiAuthorization("BUGS", "CREATE", "SELF"), ApiValidator(reportBugValidation), this._reportBug);
    this.router.post("/sentiment", ApiAuthentication, ApiAuthorization("BUGS", "CREATE", "SELF"), ApiValidator(reportSentimentValidation), this._reportSentiment);
    this.router.patch("/bug", ApiAuthentication, ApiAuthorization("BUGS", "UPDATE", "ALL"), ApiValidator(reportSolveBug), this._resolveBug);
    this.router.patch("/sentiment", ApiAuthentication, ApiAuthorization("BUGS", "UPDATE", "ALL"), ApiValidator(reportSolveSentiment), this._resolveSentiment);
  }

  private _getReports = async (req: Request, res: Response<GetReportResponseDto>) => {
    const reports = await this._reportsService.getReports();
    res.json(reports);
  }

  private _getMyReports = async (req: Request, res: Response<GetReportResponseDto>) => {
    const { userid } = req.session;
    const reports = await this._reportsService.getReports(userid);
    res.json(reports);
  }

  private _reportBug = async (req: Request<{}, {}, ReportBugRequestDto>, res: Response) => {
    const { text } = req.body;
    const { userid } = req.session;
    await this._reportsService.reportBug({ text, userId: userid });
    res.json();
  }

  private _reportSentiment = async (req: Request<{}, {}, ReportSentimentRequetsDto>, res: Response) => {
    const { sentimentId } = req.body;
    const { userid } = req.session;
    await this._reportsService.reportSentiment({ sentimentId, userId: userid });
    res.json();
  }

  private _resolveBug = async (req: Request<{}, {}, BugSolveRequestDto>, res: Response) => {
    const { id, comment, accept } = req.body;
    await this._reportsService.resolveBug({ id, comment, accept });
    res.json();
  }

  private _resolveSentiment = async (req: Request<{}, {}, SentimentSolveRequetsDto>, res: Response) => {
    const { id, comment, accept } = req.body;
    await this._reportsService.resolveSentiment({ id, comment, accept });
    res.json();
  }
}

export default ReportsController;
