import { Inject, Service } from "typedi";
import { Report, BugReport } from "@prisma/client";
import { CreateBugReport, CreateSentimentReport, ResolveBugReport, ResolveSentimentReport } from "./interfaces";
import { Database } from "@server/Utils";

@Service()
class ReportsRepository {
  @Inject()
  private readonly _db: Database;

  public getReports = async (userId?: string): Promise<(BugReport | Report)[]> => {
    const where = userId ? { where: { userId } } : null;
    const bugReports = await this._db.client.bugReport.findMany(where);
    const reports = await this._db.client.report.findMany({ ...where, include: { sentiment: true } });
    return [...bugReports, ...reports];
  }

  public createReportBug = async (data: CreateBugReport) => {
    await this._db.client.bugReport.create({ data });
  }

  public createReportSentiment = async (data: CreateSentimentReport) => {
    await this._db.client.report.create({ data });
  }

  public resolveBugReport = async ({ id, comment, accept }: ResolveBugReport) => {
    await this._db.client.bugReport.update({ where: { id }, data: { comment, status: accept, dateSolution: new Date(Date.now()) } });
  }

  public reslolveReportSentiment = async ({ id, comment, accept }: ResolveSentimentReport) => {
    await this._db.client.report.update({ where: { id }, data: { comment, status: accept, dateSolution: new Date(Date.now()) } });
  }
}

export default ReportsRepository;
