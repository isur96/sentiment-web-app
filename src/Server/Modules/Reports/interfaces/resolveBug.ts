export interface ResolveBugReport {
  id: string,
  comment?: string,
  accept: boolean,
}

export interface ResolveSentimentReport {
  id: string,
  comment?: string,
  accept: boolean,
}
