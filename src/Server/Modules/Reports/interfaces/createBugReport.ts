export interface CreateBugReport {
  text: string,
  userId: string,
}

export interface CreateSentimentReport {
  userId: string,
  sentimentId: number,
}

