import { Inject, Service } from "typedi";
import { CreatePermission } from "./interfaces";
import { PermissionRepository } from "./permissions.repository";
import { PermissionObject } from "@shared/Interfaces/permission.interface";
import HTTPError from "@server/HttpErrors";

@Service()
export class PermissionService {
  @Inject()
  private readonly _permissionRepository: PermissionRepository;

  public createPermission = async (data: CreatePermission): Promise<PermissionObject> => {
    try {
      return await this._permissionRepository.createPermission(data);
    } catch(e) {
      throw new HTTPError.BadRequest("Permission already exists");
    }
  }

  public deletePermission = async (id: string) => {
    try {
      await this._permissionRepository.deletePermission(id);
    } catch(e) {
      throw new HTTPError.NotFound();
    }
  }

  public getPermission = async (): Promise<PermissionObject[]> => {
    return await this._permissionRepository.getPermission();
  }
}
