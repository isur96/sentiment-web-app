import { Permission } from "@prisma/client";
import { Inject, Service } from "typedi";
import { CreatePermission } from "./interfaces";
import { Database } from "@server/Utils";

@Service()
export class PermissionRepository {
  @Inject()
  private readonly _db: Database

  public createPermission = async (data: CreatePermission): Promise<Permission> => {
    return await this._db.client.permission.create({ data });
  }

  public deletePermission = async (id: string) => {
    await this._db.client.permission.delete({ where: { id } });
  }

  public getPermission = async (): Promise<Permission[]> => {
    return await this._db.client.permission.findMany();
  }
}
