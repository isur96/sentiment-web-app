import { body, param } from "express-validator";

export const createValidation = [
  body("subject").isString()
    .notEmpty(),
  body("action").isString()
    .notEmpty(),
];

export const deleteValidation = [
  param("id").isString()
    .notEmpty(),
];
