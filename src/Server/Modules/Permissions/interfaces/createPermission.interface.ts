export interface CreatePermission {
  subject: string,
  action: string,
}
