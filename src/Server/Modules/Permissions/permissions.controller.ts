import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import BaseController from "../BaseController";
import { PermissionService } from "./permissions.service";
import { createValidation, deleteValidation } from "./validations";
import { API } from "@shared/Constants";
import { CreatePermissionRequestDto, CreatePermissionResponseDto, GetPermissionResponesDto } from "@shared/ApiDto/permission.dto";
import { ApiAuthentication, ApiValidator } from "@server/Middlewares";
import ApiAuthorization from "@server/Middlewares/ApiAuthorization";

@Service()
export class PermissionController extends BaseController {
  public basePath = `/${API.PERMISSIONS}`;

  @Inject()
  private readonly _permissionService: PermissionService;

  public constructor() {
    super();
    this._initRoutes();
  }

  protected _initRoutes = () => {
    this.router.post("/", ApiAuthentication, ApiAuthorization("PERMISSION", "CREATE", "ALL"), ApiValidator(createValidation), this._createPermission);
    this.router.delete("/:id", ApiAuthentication, ApiAuthorization("PERMISSION", "DELETE", "ALL"), ApiValidator(deleteValidation), this._deletePermission);
    this.router.get("/", ApiAuthentication, ApiAuthorization("PERMISSION", "READ", "ALL"), this._getPermissions);
  }

  private _createPermission = async (req: Request<{}, {}, CreatePermissionRequestDto>, res: Response<CreatePermissionResponseDto>) => {
    const permission = await this._permissionService.createPermission(req.body);
    res.json(permission);
  }

  private _deletePermission = async (req: Request<{ id: string }, {}, {}>, res: Response) => {
    await this._permissionService.deletePermission(req.params.id);
    res.json();
  }

  private _getPermissions = async (req: Request, res: Response<GetPermissionResponesDto>) => {
    const permissions = await this._permissionService.getPermission();
    res.json(permissions);
  }
}
