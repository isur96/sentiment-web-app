import Container from "typedi";

import * as AuthModule from "./Auth";
import * as UsersModule from "./Users";
import * as SessionModule from "./Sessions";
import * as SettingsModule from "./Settings";
import * as SentimentModule from "./Sentiment";
import * as PermissionsModule from "./Permissions";
import * as RolesModule from "./Roles";
import * as ReportsModule from "./Reports";

import BaseController from "./BaseController";

export const Controllers: BaseController[] = [
  Container.get(AuthModule.AuthController),
  Container.get(UsersModule.UsersController),
  Container.get(SettingsModule.SettingsController),
  Container.get(SentimentModule.SentimentController),
  Container.get(RolesModule.RolesController),
  Container.get(PermissionsModule.PermissionController),
  Container.get(ReportsModule.ReportsController),
];

export {
  AuthModule,
  UsersModule,
  SessionModule,
  SettingsModule,
  SentimentModule,
  PermissionsModule,
  RolesModule,
};
