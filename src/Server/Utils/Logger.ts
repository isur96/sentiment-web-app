import chalk from "chalk";
/* eslint-disable no-console */
class Logger {
  public static Log(data: unknown, depth = false) {
    if(depth) {
      console.dir(data, { depth: null });
    } else {
      console.log(data);
    }
  }

  public static Error(data: unknown) {
    console.error(data);
  }

  public static ErrorHandler(data: unknown, expected?: unknown) {
    // TODO: handle some error logs.
    console.log(chalk.bgRed.white("vvvvvvvvvvvvvvvvvvvvvvvvvvvv"));
    const toLog: { data: unknown, expected?: unknown } = { data };
    if(expected) toLog.expected = expected;
    console.dir(toLog, { depth: null });
    console.log(chalk.bgRed.white("^^^^^^^^^^^^^^^^^^^^^^^^^^^"));
  }
}
/* eslint-enable no-console */
export default Logger;
