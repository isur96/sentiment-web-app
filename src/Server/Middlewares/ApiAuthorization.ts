import { NextFunction, Request, Response } from "express";
import { MiddlewareFunction } from "./Middleware.interface";
import HttpErrors from "@server/HttpErrors";
import { PermissionActions, PermissionSubjects, PermissionType } from "@shared/Interfaces/permission.interface";
import { permissionLevelCompare } from "@shared/Utils/PermissionHelper";

class ApiAuthorization implements MiddlewareFunction {
  public execute = (subject: PermissionSubjects, action: PermissionActions, type: PermissionType) => async (req: Request, res: Response, next: NextFunction) => {
    if(req.session.roles.includes("ADMIN")) return next();

    const permission = req.session.permissions.find(perm => perm.action === action && perm.subject === subject);

    if(!permission || !permissionLevelCompare(permission.type, type)) throw new HttpErrors.Forbidden();
    next();
  }
}

export default new ApiAuthorization().execute;
