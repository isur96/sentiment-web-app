import { NextFunction, Request, Response } from "express";
import Container from "typedi";
import { SessionService } from "../Modules/Sessions";
import JWT from "../Utils/JWT";
import { Middleware } from "./Middleware.interface";
import { UsersService } from "@server/Modules/Users";

class SessionMiddleware implements Middleware {
  private readonly _jwt: JWT;
  private readonly _sessionService: SessionService;
  private readonly _usersService: UsersService;
  public constructor() {
    this._jwt = Container.get(JWT);
    this._sessionService = Container.get(SessionService);
    this._usersService = Container.get(UsersService);
  }

  public execute = async (req: Request, res: Response, next: NextFunction) => {
    const token = this._getToken(req);

    try {
      const decoded = this._jwt.tokenVerify(token);
      const session = await this._sessionService.getSession(decoded.sessionid);
      const user = await this._usersService.findUser({ search: { field: "id", search: session.userId }, permissions: true });
      req.session = {
        userid: session.userId,
        id: session.sessionId,
        permissions: user.permissions,
        roles: user.roles,
      };
    } catch(error) {
      req.session = {
        userid: null,
        id: null,
        permissions: [],
        roles: [],
      };
    } finally {
      next();
    }
  }

  private _getToken = (req: Request): string => {
    if(req.cookies.jwt) {
      return req.cookies.jwt;
    } else {
      return req.get("auth-token");
    }
  }
}

export default new SessionMiddleware().execute;
