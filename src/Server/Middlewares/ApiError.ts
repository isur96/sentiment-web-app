import { NextFunction, Request, Response } from "express";
import { Logger } from "../Utils";
import { HTTPError } from "../HttpErrors/HTTPError";
import { MiddlewareError } from "./Middleware.interface";

class ApiError implements MiddlewareError {
  public execute = (error: HTTPError, req: Request, res: Response, next: NextFunction) => {
    Logger.Error(error);
    const code = parseInt(error.code.toString()) || 500;
    const message = !code || code === 500 ? null : error.message;
    res.status(code).json({ code, message });
  }
}

export default new ApiError().execute;
