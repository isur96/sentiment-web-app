import { paramsCreator, ParamsObject } from "./paramsCreator";

describe("Params creator", () => {
  it("Should correctly create empty params string", () => {
    const expected = "";
    const paramsObject = {};
    const result = paramsCreator(paramsObject);
    expect(result).toBe(expected);
  });

  it("Should correctly create params string", () => {
    const expected = "?search=ad&permissions=true&num=23";
    const paramsObject: ParamsObject = {
      search: "ad",
      permissions: true,
      num: 23,
      test: undefined,
      test2: "",
    };
    const result = paramsCreator(paramsObject);
    expect(result).toBe(expected);
  });
});
