export interface ParamsObject {
  [key: string]: string | number | boolean | undefined,
}

export const paramsCreator = (params: ParamsObject): string => {
  const keys = Object.keys(params);
  const pathArray = [];
  for(const key of keys) {
    if(params[key] !== undefined && params[key] !== "") {
      pathArray.push(`${key}=${params[key].toString()}`);
    }
  }
  const path = pathArray.join("&");
  return pathArray.length > 0 ? `?${path}` : "";
};
