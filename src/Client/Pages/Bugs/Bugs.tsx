import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Row from "./Row";
import { Layout } from "@client/Containers";
import { ReportsService } from "@client/Services";
import { GetReportResponseDto } from "@shared/ApiDto/bugs.dto";
import { AppState } from "@shared/Redux/store";
import "./Bugs.scss";

const BugsPage = () => {
  const [reports, setReports] = useState<GetReportResponseDto>([]);
  const permission = useSelector((state: AppState) => state.localUser.permissions);

  useEffect(() => {
    reload();
  }, []);

  const reload = () => {
    const perm = permission.find(item => item.type === "ALL");
    if(perm) getReports();
    else getMyReports();
  };

  const getReports = async () => {
    const r = await ReportsService.getReports();
    setReports(r);
  };

  const getMyReports = async () => {
    const r = await ReportsService.getMyReports();
    setReports(r);
  };

  return (
    <Layout>
      <div className="BugsPage">
        <table>
          <thead>
            <tr>
              <th> Sentiment Id </th>
              <th> Text </th>
              <th> Value </th>
              <th> Date </th>
              <th> Action </th>
            </tr>
          </thead>
          <tbody>
            {reports.map(report =>
              <Row reload={reload} key={report.id} comment={report.comment} dateSolution={report.dateSolution} status={report.status} date={report.date} id={report.id} sentiment={report.sentiment} text={report.text} sentimentId={report.sentimentId} />,
            )}
          </tbody>
        </table>
      </div>
    </Layout>
  );
};

export default BugsPage;
