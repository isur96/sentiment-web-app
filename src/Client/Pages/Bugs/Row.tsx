import React, { FunctionComponent, useState } from "react";
import { Button, Guard, Input } from "@client/Components";
import { ReportsService } from "@client/Services";

interface Props {
  id: string,
  sentimentId: number,
  text: string,
  sentiment: number,
  date: Date,
  status: boolean,
  dateSolution: Date,
  comment: string,
  reload: () => void,
}

const Row: FunctionComponent<Props> = report => {
  const [comment, setComment] = useState<string>("");

  const handleReport = async (accept: boolean) => {
    if(report.sentimentId) {
      await ReportsService.solveSentiment({ id: report.id, comment, accept });
    } else {
      await ReportsService.solveBug({ id: report.id, comment, accept });
    }

    report.reload();
  };

  return (
    <tr key={report.id}>
      <td> {report.sentimentId} </td>
      <td> {report.text} </td>
      <td> {report.sentiment} </td>
      <td> {new Date(report.date).toLocaleString()} </td>
      <td>
        {!report.dateSolution
        ? <Guard subject="BUGS" action="UPDATE" type="ALL">
          <Input label="Comment" value={comment} onChange={e => setComment(e.target.value)} />
          <Button content="Accept" onClick={() => handleReport(true)} />
          <Button content="Decline" onClick={() => handleReport(false)} />
        </Guard>
        : <> {new Date(report.dateSolution).toLocaleString()} - {report.status ? "Accepted" : "Declined"} - {report.comment} </>
}
      </td>
    </tr>
  );
};

export default Row;
