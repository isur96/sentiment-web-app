import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { FcCancel, FcCheckmark, FcMinus } from "react-icons/fc";
import { Layout } from "@client/Containers";
import { SentimentService } from "@client/Services";
import { SentimentObject } from "@shared/Interfaces/sentiment.interface";
import { sentimentToResult } from "@shared/Utils/sentimentToResult";
import { Button, Guard } from "@client/Components";
import "./HistoryPage.scss";

const HistoryPage = () => {
  const [sentiments, setSentiments] = useState<SentimentObject[]>([]);
  const [all, setAll] = useState<boolean>(false);
  const { t } = useTranslation("historyPage");

  useEffect(() => {
    loadSentiments();
  }, []);

  useEffect(() => {
    if(all) {
      loadAll();
    } else {
      loadSentiments();
    }
  }, [all]);

  const loadSentiments = async () => {
    const sentiments = await SentimentService.getSentiments();
    setSentiments(sentiments);
  };

  const sentimentToIcon = (sentiment: "POSITIVE" | "NEGATIVE" | "NEUTRAL") => {
    if(sentiment === "NEGATIVE") return <FcCancel />;
    if(sentiment === "NEUTRAL") return <FcMinus />;
    return <FcCheckmark />;
  };

  const loadAll = async () => {
    const sentiments = await SentimentService.getAllSentiments();
    setSentiments(sentiments);
  };

  return (
    <Layout>
      <div className="Page HistoryPage">
        <Guard subject="SENTIMENTS" action="READ" type="ALL">
          <Button content={all ? t("my") : t("all")} onClick={() => setAll(!all)} />
        </Guard>
        <div className="tab">
          <table>
            <thead>
              <tr>
                <th> {t("text")} </th>
                <th> {t("numeric")} </th>
                <th> {t("result")} </th>
                <th> {t("date")} </th>
              </tr>
            </thead>
            <tbody>
              {sentiments.map(sent =>
                <tr key={`${sent.date}${sent.sentiment}`}>
                  <td> {sent.text}</td>
                  <td> {sent.sentiment && sent.sentiment.toFixed(4)} </td>
                  <td> {sentimentToIcon(sentimentToResult(sent.sentiment))} </td>
                  <td> {new Date(sent.date).toLocaleString()} </td>
                </tr>
                ,
                )}
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default HistoryPage;
