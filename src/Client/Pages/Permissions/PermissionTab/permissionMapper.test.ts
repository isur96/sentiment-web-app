import { groupPermissions } from "./permissionMapper";

describe("Permission mapper", () => {
  const examplePermissions = [
    {
      "id": "68e7d10d-0058-4bf7-8dbe-81a477b62651",
      "action": "create",
      "subject": "permission",
    },
    {
      "id": "92c956e7-5753-4107-98d9-c3300cca9d04",
      "action": "delete",
      "subject": "permission",
    },
    {
      "id": "39c68879-958d-4f7e-b462-ea123aeb25ec",
      "action": "update",
      "subject": "permission",
    },
  ];
  const exampleResult = {
    "permission": [
      {
        "id": "68e7d10d-0058-4bf7-8dbe-81a477b62651",
        "name": "create",
      },
      {
        "id": "92c956e7-5753-4107-98d9-c3300cca9d04",
        "name": "delete",
      },
      {
        "id": "39c68879-958d-4f7e-b462-ea123aeb25ec",
        "name": "update",
      },
    ],
  };
  it("Should correctly map permissions", () => {
    const res = groupPermissions(examplePermissions);
    expect(res).toMatchObject(exampleResult);
  });
});
