import React, { FunctionComponent } from "react";
import { useTranslation } from "react-i18next";
import { GroupPermission } from "./permissionMapper";
import { RoleObject } from "@shared/Interfaces/role.interface";
import { TYPES } from "@shared/Constants/Permissions";
import { PermissionType } from ".prisma/client";

interface Props {
  role: RoleObject,
  permissions: GroupPermission,
  handleAssignPermission: (role: string, permission: string, type: PermissionType) => Promise<void>,
  handleUnAssignPermission: (role: string, permission: string) => Promise<void>,
}

const RoleRow: FunctionComponent<Props> = ({ handleAssignPermission, handleUnAssignPermission, permissions, role }) => {
  return (
    <tr className="UserRow">
      <td> {role.name} </td>
      {Object.keys(permissions).map(key => {
        return permissions[key].map(perm => {
          const val = role.permissions.find(p => p.permissionId === perm.id);
          const value = val ? val.type : undefined;
          return <td key={perm.id}> <Dropdown value={value} role={role.id} permission={perm.id} handleAssignPermission={handleAssignPermission} handleUnAssignPermission={handleUnAssignPermission} /> </td>;
        });
      })}
    </tr>
  );
};

const Dropdown: FunctionComponent<{
  role: string,
  permission: string,
  handleAssignPermission: (role: string, permission: string, type: PermissionType) => Promise<void>,
  handleUnAssignPermission: (role: string, permission: string) => Promise<void>,
  value: PermissionType | undefined,
}> = ({ role, permission, handleAssignPermission, handleUnAssignPermission, value }) => {
  const { t } = useTranslation("permissionPage");
  const options = Object.keys(TYPES).map((key: keyof typeof TYPES) => ({ text: TYPES[key], value: TYPES[key] }));
  const handleChange = async (val: PermissionType | "None") => {
    if(val === "None") {
      await handleUnAssignPermission(role, permission);
    } else {
      await handleAssignPermission(role, permission, val);
    }
  };
  return (
    <select value={value ? value : "None"} onChange={ev => handleChange(ev.target.value as PermissionType | "None")}>
      {options.map(opt => <option key={opt.value} value={opt.value}> {opt.text} </option>)}
      <option value="None"> {t("none")} </option>
    </select>
  );
};

export default RoleRow;
