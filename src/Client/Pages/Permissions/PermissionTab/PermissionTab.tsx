import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { GroupPermission, groupPermissions } from "./permissionMapper";
import RoleRow from "./RoleRow";
import { PermissionsService, RolesService } from "@client/Services";
import { RoleObject } from "@shared/Interfaces/role.interface";
import { PermissionType } from ".prisma/client";
import "./PermissionTab.scss";

const PermissionsTab = () => {
  const [roles, setRoles] = useState<RoleObject[]>([]);
  const [permissions, setPermissions] = useState<GroupPermission>({});
  const { t } = useTranslation("permissionPage");

  useEffect(() => {
    loadRoles();
    loadPermissions();
  }, []);

  const loadRoles = async () => {
    const roles = await RolesService.getRoles();
    setRoles(roles);
  };

  const loadPermissions = async () => {
    const permissions = await PermissionsService.getPermissions();
    const mapPermission = groupPermissions(permissions);
    setPermissions(mapPermission);
  };

  const hadnleAssignPermission = async (role: string, permission: string, type: PermissionType) => {
    await RolesService.assignPermission(role, permission, type);
    await loadPermissions();
    await loadRoles();
  };

  const handleUnAssignPermission = async (role: string, permission: string) => {
    await RolesService.unAssignPermission(role, permission);
    await loadPermissions();
    await loadRoles();
  };

  return (
    <div className="PermissionsTab">
      <table>
        <thead>
          <tr>
            <th rowSpan={2}> {t("roleName")} </th>
            {Object.keys(permissions).map(key => <th key={key} colSpan={permissions[key].length}> {key} </th>)}
          </tr>
          <tr>
            {Object.keys(permissions).map(key =>  permissions[key].map(perm => <th key={perm.id}> {perm.name} </th>))}
          </tr>
        </thead>
        <tbody>
          {roles.map(role => <RoleRow key={role.id} handleAssignPermission={hadnleAssignPermission} handleUnAssignPermission={handleUnAssignPermission} permissions={permissions} role={role} />)}
        </tbody>
      </table>
    </div>
  );
};
export default PermissionsTab;
