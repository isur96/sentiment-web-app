import { PermissionType } from ".prisma/client";
import { PermissionObject } from "@shared/Interfaces/permission.interface";

export const groupPermissions = (permissions: PermissionObject[]): GroupPermission => {
  const group: GroupPermission = {};
  permissions.reduce((prev, curr) => {
    if(prev[curr.subject]) {
      prev[curr.subject].push({ id: curr.id, name: curr.action, type: curr.type });
    } else {
      prev[curr.subject] = [{ id: curr.id, name: curr.action, type: curr.type }];
    }
    return prev;
  }, group);

  return group;
};

export interface GroupPermission {
  [key: string]: {
    id: string,
    name: string,
    type: PermissionType,
  }[],
}
