import React from "react";
import { useTranslation } from "react-i18next";
import RoleTab from "./RoleTab";
import PermissionsTab from "./PermissionTab";
import { Segment } from "@client/Components";
import { Layout } from "@client/Containers";
import "./PermissionsPage.scss";

const PermissionsPage = () => {
  const { t } = useTranslation("permissionPage");
  return (
    <Layout>
      <div className="Page PermissionsPage">
        <Segment tabs={[
          { name: t("Roles"), child: <RoleTab /> },
          { name: t("Permissions"), child: <PermissionsTab /> },
          ]} />
      </div>
    </Layout>
  );
};

export default PermissionsPage;
