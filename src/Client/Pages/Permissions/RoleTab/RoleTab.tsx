import React, { FunctionComponent, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import UserRow from "./UserRow";
import { RolesService, UsersService } from "@client/Services";
import { RoleObject } from "@shared/Interfaces/role.interface";
import { UserObject } from "@shared/Interfaces/user.interface";
import { Input } from "@client/Components";
import "./RoleTab.scss";

const RoleTab = () => {
  const [users, setUsers] = useState<UserObject[]>([]);
  const [roles, setRoles] = useState<RoleObject[]>([]);
  const [search, setSearch] = useState<string>("");
  const { t } = useTranslation("permissionPage");

  useEffect(() => {
    loadRoles();
    loadUsers();
  }, []);

  useEffect(() => {
    loadUsers(search);
  }, [search]);

  const loadUsers = async (search?: string) => {
    const users = await UsersService.getUsers(search, true);
    setUsers(users);
  };

  const loadRoles = async () => {
    const roles = await RolesService.getRoles();
    setRoles(roles);
  };

  const handleAssignRole = async (user: string, role: string) => {
    await UsersService.assignRole(user, role);
    await loadUsers();
  };

  const handleUnAssignRole = async (user: string, role: string) => {
    await UsersService.unAssignRole(user, role);
    await loadUsers();
  };

  return (
    <div className="RoleTab">
      <Input placeholder={t("roleSearch")} onChange={e => setSearch(e.target.value)} value={search} />
      <table>
        <thead>
          <tr>
            <td> {t("userName")} </td>
            {roles.map(role => <td key={role.id}> {role.name} </td>)}
          </tr>
        </thead>
        <tbody>
          {users.map(user => <UserRow key={user.id} handleAssignRole={handleAssignRole} handleUnAssignRole={handleUnAssignRole} user={user} roles={roles} />)}
        </tbody>
      </table>
    </div>
  );
};

export default RoleTab;
