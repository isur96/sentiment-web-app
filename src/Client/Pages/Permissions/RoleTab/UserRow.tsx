import React, { FunctionComponent } from "react";
import { RiCheckFill, RiForbid2Line } from "react-icons/ri";
import { RoleObject } from "@shared/Interfaces/role.interface";
import { UserObject } from "@shared/Interfaces/user.interface";

interface Props {
  user: UserObject,
  roles: RoleObject[],
  handleAssignRole: (user: string, role: string) => void,
  handleUnAssignRole: (user: string, role: string) => void,
}

const UserRow: FunctionComponent<Props> = ({ user, roles, handleAssignRole, handleUnAssignRole }) => {
  return (
    <tr className="UserRow">
      <td> {user.email} </td>
      {roles.map(role =>
        <td key={role.id}>
          {user.roles.includes(role.name) ? <RiCheckFill color="green" onClick={() => handleUnAssignRole(user.id, role.id)} /> : <RiForbid2Line color="red" onClick={() => handleAssignRole(user.id, role.id)} />}
        </td>,
      )}
    </tr>
  );
};

export default UserRow;
