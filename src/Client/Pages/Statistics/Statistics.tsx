import React, { FunctionComponent, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import isNode from "detect-node";
import { useSelector } from "react-redux";
import { Bar, BarChart, CartesianGrid, Cell, Legend, Pie, PieChart, Tooltip, XAxis, YAxis } from "recharts";
import { Layout } from "@client/Containers";
import { SentimentService } from "@client/Services";
import { Button, Guard } from "@client/Components";
import { AppState } from "@shared/Redux/store";
import "./Statistics.scss";

interface ChartData {
  name: string,
  value: number,
}
const COLORS = [
  "#FF8042",
  "#00C49F",
  "#0088FE",
];
const RADIAN = Math.PI / 180;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const renderCustomizedLabel: FunctionComponent<any> = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? "start" : "end"} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

const Statistics = () => {
  if(isNode) return null;
  const [sentiments, setSentiments] = useState<ChartData[]>([]);
  const [byMonths, setByMonths] = useState<ChartData[]>([]);
  const [all, setAll] = useState<number>(0);
  const [allStats, setAllStats] = useState<boolean>(false);
  const userId = useSelector((state: AppState) => state.localUser.id);
  const { t } = useTranslation("statisticPage");
  useEffect(() => {
    load(allStats);
  }, [allStats]);

  const load = async (all: boolean) => {
    const stats = await SentimentService.getStatistics(all ? undefined : userId);
    setSentiments(sentimentToArray(stats.bySentiment));
    setByMonths(sentimentToArray(stats.byMonths));
    setAll(stats.all);
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const sentimentToArray = (object: any): ChartData[] => {
    return Object.keys(object).map(key => ({ name: key, value: object[key] }));
  };

  return (
    <Layout>
      <div className="StatisticsPage">
        <Guard subject="STATISTICS" action="READ" type="ALL">
          <Button content={allStats ? "All" : "Self"} onClick={() => setAllStats(allStats => !allStats)} />
        </Guard>
        <div>
          <h1> {t("headers.bySentiment")} </h1>
          <PieChart width={500} height={500}>
            <Pie legendType="circle"
                 label={renderCustomizedLabel}
                 labelLine={false}
                 data={sentiments}
                 dataKey="value"
                 nameKey="name"
                 cx="50%"
                 cy="50%"
                 outerRadius={150}
                 fillRule="nonzero">
              {sentiments.map((entry, index) =>
                <Cell key={index} fill={COLORS[index % COLORS.length]} />,
            )}
            </Pie>
            <Legend />
          </PieChart>
        </div>
        <div>
          <h1> {t("headers.byMonth")} </h1>
          <BarChart width={500}
                    height={500}
                    data={byMonths}>
            <Bar dataKey="value" fill="#8884d8" />
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
          </BarChart>
        </div>
        <div>
          <h1> {t("headers.bySentiment")} </h1>
          <table>
            <thead>
              <tr>
                <th> {t("headers.sentiment")} </th>
                <th> {t("headers.count")} </th>
              </tr>
            </thead>
            <tbody>
              {sentiments.map(stat =>
                <tr key={stat.name}>
                  <td> {stat.name} </td>
                  <td> {stat.value} </td>
                </tr>,
                  )}
              <tr>
                <th> {t("headers.total")} </th>
                <th> {all} </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default Statistics;
