export { LoginPage, RegisterPage } from "./Auth";
export { default as Homepage } from "./Homepage";
export { default as ErrorPage } from "./Error";
export { default as PermissionsPage } from "./Permissions";
export { default as HistoryPage } from "./History";
export { default as ProfilePage } from "./Profile";
export { default as StatisticsPage } from "./Statistics";
export { default as BugsPage } from "./Bugs";
