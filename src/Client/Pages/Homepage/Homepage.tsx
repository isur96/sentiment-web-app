import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Alert, Button, Form, Guard, Input, Modal } from "@client/Components";
import { Layout } from "@client/Containers";
import { SentimentService, ReportsService } from "@client/Services";
import { sentimentToResult } from "@shared/Utils/sentimentToResult";
import "./Homepage.scss";
import { AlertType } from "@client/Components/Alert/AlertProps.interface";

interface SentimentForm {
  text: string,
}

const Homepage = () => {
  const [result, setResult] = useState<{ text: string, result: string, id: number }>();
  const [error, setError] = useState<boolean>(false);
  const [openReport, setOpenReport] = useState<boolean>(false);
  const { t } = useTranslation("homepage");

  const handleCheck = async (data: SentimentForm) => {
    try {
      setError(false);
      const response = await SentimentService.checkSentiment(data);
      setResult({
        result: sentimentToResult(response.sentiment),
        text: response.text,
        id: response.id,
      });
    } catch{
      setError(true);
    }
  };

  const resolveAlertType = (result: string): AlertType => {
    switch(result) {
      case "POSITIVE":
        return "success";
      case "NEGATIVE":
        return "error";
      default:
        return "info";
    }
  };

  const handleReportClick = () => {
    setOpenReport(true);
  };

  const handleCloseReport = () => {
    setOpenReport(false);
  };

  const reportResult = async () => {
    await ReportsService.reportSentiment({ sentimentId: result.id });
    handleCloseReport();
  };

  return (
    <Layout>
      <div className="Page Homepage">
        <Guard subject="SENTIMENTS" action="CREATE">
          <h2> {t("info-header")} </h2>
          <p> {t("info")} </p>
          <Form<SentimentForm> onSubmit={handleCheck} validation={{}}>
            <Input label="" name="text" placeholder={t("inputPlaceholder")} />
            <Button content={t("check")} />
          </Form>
          {result
            && <Alert type={resolveAlertType(result.result)}>
              <div className="res">
                {t("result", {
                  result: t(result.result),
                  text: result.text,
                })}
                <Button content="Report" onClick={handleReportClick} />
              </div>
            </Alert>
          }
          {error && <Alert type="error"> Nie dziaua </Alert>}
          <Modal title="Report" text="Are you sure?" action={reportResult} onClose={handleCloseReport} open={openReport} />
        </Guard>
      </div>
    </Layout>
  );
};

export default Homepage;
