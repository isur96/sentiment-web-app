import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Input } from "@client/Components";
import { Layout } from "@client/Containers";
import { AppState } from "@shared/Redux/store";
import "./Profile.scss";
import { updateLocalUser } from "@shared/Redux/LocalUser/actions";

interface ProfileForm {
  username: string,
  email: string,
}

const Profile = () => {
  const dispatch = useDispatch();
  const { createdDate, email, updateDate, username } = useSelector((state: AppState) => state.localUser);
  const defaultValues = { email, username };
  const { t } = useTranslation(["profilePage", "common"]);
  const handleSubmit = (data: ProfileForm) => {
    dispatch(updateLocalUser(data));
  };
  return (
    <Layout>
      <div className="Profile">
        <Form<ProfileForm> className="profileForm" validation={{}} onSubmit={handleSubmit} defaultValues={defaultValues} translation="profilePage">
          <h2> {t("profileData")} </h2>
          <Input name="email" type="email" />
          <Input name="username" />
          <Input disabled value={createdDate.toLocaleString()} label={t("createDate")} />
          <Input disabled value={updateDate.toLocaleString()} label={t("updateDate")} />
          <Button content={t("common:submit")} type="submit" />
        </Form>
      </div>
    </Layout>
  );
};

export default Profile;
