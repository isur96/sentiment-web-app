import { ApiService } from "./Base/api.service";
import { SentimentAnalyzeResponseDto, SentimentAnalyzeRequestDto, SentimentGetResponseDto, StatisticsResponseDto } from "@shared/ApiDto/sentiment.dto";

class SentimentService extends ApiService {
  public checkSentiment = async (data: SentimentAnalyzeRequestDto): Promise<SentimentAnalyzeResponseDto> => {
    return await this.requestService.post<SentimentAnalyzeResponseDto, SentimentAnalyzeRequestDto>("/", data);
  }

  public getSentiments = async (): Promise<SentimentGetResponseDto> => {
    return await this.requestService.get<SentimentGetResponseDto>("/my");
  }

  public getAllSentiments = async (): Promise<SentimentGetResponseDto> => {
    return await this.requestService.get<SentimentGetResponseDto>("");
  }

  public getStatistics = async (id: string): Promise<StatisticsResponseDto> => {
    return await this.requestService.get<StatisticsResponseDto>(id ? `/statistics/${id}` : "/statistics");
  }
}

export default SentimentService;
