import { ApiService } from "./Base/api.service";
import { GetUsersDtoResponse, MeResponseDto, UpdateuserRequestDto, UpdateUserResponseDto } from "@shared/ApiDto/users.dto";
import { paramsCreator } from "@client/Utils/paramsCreator";

class UsersService extends ApiService {
  public async getUsers(search: string, permissions: boolean): Promise<GetUsersDtoResponse> {
    const params = { search, permissions };
    const paramsPath = paramsCreator(params);
    return await this.requestService.get(`/${paramsPath}`);
  }

  public getMe = async (): Promise<MeResponseDto> => {
    return await this.requestService.get<MeResponseDto>("/me");
  }

  public updateUser = async (data: UpdateuserRequestDto): Promise<UpdateUserResponseDto> => {
    return await this.requestService.patch<UpdateUserResponseDto>("", data);
  }

  public assignRole = async (user: string, role: string): Promise<void> => {
    return await this.requestService.patch(`/${user}/assignRole`, { role });
  };

  public unAssignRole = async (user: string, role: string): Promise<void> => {
    return await this.requestService.patch(`/${user}/unAssignRole`, { role });
  };
}

export default UsersService;
