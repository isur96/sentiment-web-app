import { ApiService } from "./Base/api.service";
import { AssignPermissionRequestDto, AssignPermissionResponseDto, GetRolesResponseDto, UnAssignPermissionRequestDto, UnAssignPermissionResponseDto } from "@shared/ApiDto/role.dto";
import { PermissionType } from ".prisma/client";

class RolesService extends ApiService {
  public async getRoles(): Promise<GetRolesResponseDto> {
    return await this.requestService.get(`/`);
  }

  public assignPermission = async (roleId: string, permissionId: string, type: PermissionType) => {
    return await this.requestService.patch<AssignPermissionResponseDto, AssignPermissionRequestDto>(`/${roleId}/assign`, { permission: permissionId, type });
  }

  public unAssignPermission = async (roleId: string, permissionId: string) => {
    return await this.requestService.patch<UnAssignPermissionResponseDto, UnAssignPermissionRequestDto>(`/${roleId}/unAssign`, { permission: permissionId });
  }
}

export default RolesService;
