import { ApiService } from "./Base/api.service";
import { GetPermissionResponesDto } from "@shared/ApiDto/permission.dto";

class PermissionsService extends ApiService {
  public async getPermissions(): Promise<GetPermissionResponesDto> {
    return await this.requestService.get(`/`);
  }
}

export default PermissionsService;
