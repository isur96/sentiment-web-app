import { ApiService } from "./Base/api.service";
import { BugSolveRequestDto, GetReportResponseDto, ReportBugRequestDto, ReportSentimentRequetsDto, SentimentSolveRequetsDto } from "@shared/ApiDto/bugs.dto";

class BugsService extends ApiService {
  public getReports = async (): Promise<GetReportResponseDto> => {
    return await this.requestService.get("/");
  }

  public getMyReports = async (): Promise<GetReportResponseDto> => {
    return await this.requestService.get("/my");
  }

  public reportSentiment = async (data: ReportSentimentRequetsDto): Promise<void> => {
    return await this.requestService.post("/sentiment", data);
  }

  public reportBug = async (data: ReportBugRequestDto): Promise<void> => {
    return await this.requestService.post("/bug", data);
  }

  public solveBug = async (data: BugSolveRequestDto): Promise<void> => {
    return await this.requestService.patch("/bug", data);
  }

  public solveSentiment = async (data: SentimentSolveRequetsDto): Promise<void> => {
    return await this.requestService.patch("/sentiment", data);
  }
}

export default BugsService;
