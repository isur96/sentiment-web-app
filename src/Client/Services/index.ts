import { default as Auth } from "./auth.service";
import { default as Settings } from "./settings.service";
import { default as Sentiment } from "./sentiment.service";
import { default as Users } from "./users.service";
import { default as Roles } from "./roles.service";
import { default as Permissions } from "./permissions.service";
import { default as Bugs } from "./bugs.service";
import { API } from "@shared/Constants";

export const AuthService =  new Auth(API.AUTH);
export const SettingsService =  new Settings(API.SETTINGS);
export const SentimentService =  new Sentiment(API.SENTIMENT);
export const UsersService =  new Users(API.USERS);
export const RolesService = new Roles(API.ROLES);
export const PermissionsService = new Permissions(API.PERMISSIONS);
export const ReportsService = new Bugs(API.REPORTS);
