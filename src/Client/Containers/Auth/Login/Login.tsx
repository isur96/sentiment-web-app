import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { LoginForm, validationSchemas } from "./LoginForm";
import { useRedirect } from "@client/Hooks";
import { Button, Input, Form, Alert } from "@client/Components";
import { login as Login } from "@shared/Redux/Auth";
import { AppState } from "@shared/Redux/store";
import { PATHS } from "@shared/Constants";
import "../Auth.scss";

const LoginContainer = () => {
  const dispatch = useDispatch();
  const redirect = useRedirect();
  const { loginError: authError, registerSuccess } = useSelector((state: AppState) => state.auth);
  const { t } = useTranslation(["loginPage", "common"]);

  const handleLogin = async (data: LoginForm) => {
    dispatch(Login(data.login, data.password));
  };

  const handleRegister = () => {
    redirect(PATHS.REGISTER);
  };

  return (
    <div className="Auth">
      <h1> {t("login")} </h1>
      <Form<LoginForm> onSubmit={handleLogin} validation={validationSchemas()} translation="loginPage">
        <Input name="login" />
        <Input name="password" type="password" />
        {authError && <Alert type="error"> {t("loginFailed")} </Alert>}
        <p> {registerSuccess && t("registerSuccess")} </p>
        <Button type="submit" content="Login" />
        <Button onClick={handleRegister} content="Register" />
      </Form>
    </div>
  );
};

export default LoginContainer;
