import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { RegisterForm, validationSchemas } from "./RegisterForm";
import { Button, Input, Form, Alert } from "@client/Components";
import useRedirect from "@client/Hooks/useRedirect";
import { PATHS } from "@shared/Constants";
import { register } from "@shared/Redux/Auth";
import { AppState } from "@shared/Redux/store";
import "../Auth.scss";

const RegisterContainer = () => {
  const redirect = useRedirect();
  const dispatch = useDispatch();
  const { registerError, registerSuccess } = useSelector((state: AppState) => state.auth);
  const { t } = useTranslation(["registerPage", "common"]);

  const handleSubmit = async (data: RegisterForm) => {
    dispatch(register(data));
  };

  const handleLogin = () => {
    redirect(PATHS.LOGIN);
  };

  if(registerSuccess) handleLogin();

  return (
    <div className="Auth">
      <h1> {t("register")} </h1>
      <Form<RegisterForm> onSubmit={handleSubmit} validation={validationSchemas()} translation="registerPage">
        <Input name="email" type="email" />
        <Input name="username" />
        <Input name="password" type="password" />
        <Input name="confirmPassword" type="password" />
        {registerError && <Alert type="error">{t("registerFailed")}</Alert>}
        <Button content="Register" type="submit" />
        <Button content="Login" onClick={handleLogin} />
      </Form>
    </div>
  );
};

export default RegisterContainer;
