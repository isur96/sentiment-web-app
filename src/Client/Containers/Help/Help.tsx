import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Button, Input } from "@client/Components";
import { ReportsService } from "@client/Services";
import { AppState } from "@shared/Redux/store";
import "./Help.scss";

const Help = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const user = useSelector((state: AppState) => state.localUser);

  if(!user.id) return null;

  const handleButtonClick = () => {
    setIsOpen(true);
  };

  const handleClose = () => setIsOpen(false);

  const handleSubmit = async () => {
    await ReportsService.reportBug({ text: message });
    handleClose();
  };

  const modalClass = `modal-container ${isOpen ? "open" : "close"}`;

  return (
    <div className="Help">
      <Button content="HELP" onClick={handleButtonClick} />
      <div className={modalClass}>
        <div className="modal">
          <div className="header"> HELP ME </div>
          <div className="content">
            <Input secondary label="Message" onChange={e => setMessage(e.target.value)} value={message} />
          </div>
          <div className="actions">
            <Button content="Send" onClick={handleSubmit} />
            <Button content="Cancel" onClick={handleClose} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Help;
