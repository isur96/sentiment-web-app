import React, { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { useRedirect } from "@client/Hooks";
import { PATHS } from "@shared/Constants";
import { logout } from "@shared/Redux/Auth";
import "./Layout.scss";
import { Guard } from "@client/Components";

const Layout: FunctionComponent = ({ children }) => {
  const redirect = useRedirect();
  const dispatch = useDispatch();
  const { t } = useTranslation("layout");

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <div className="Layout">
      <ul>
        <li onClick={handleLogout}> {t("logout")} </li>
        <li onClick={() => redirect(PATHS.HOMEPAGE)}> {t("homePage")} </li>
        <li onClick={() => redirect(PATHS.PROFILE)}> {t("profilePage")} </li>
        <li onClick={() => redirect(PATHS.HISTORY)}> {t("history")} </li>
        <li onClick={() => redirect(PATHS.STATISTICS)}> {t("statistics")} </li>
        <li onClick={() => redirect(PATHS.BUGS)}> {t("bugs")} </li>
        <Guard subject="PERMISSION" type="ALL">
          <li onClick={() => redirect(PATHS.PERMISSIONS)}> {t("permissionsPage")} </li>
        </Guard>
      </ul>
      <div className="content">
        {children}
      </div>
    </div>
  );
};

export default Layout;
