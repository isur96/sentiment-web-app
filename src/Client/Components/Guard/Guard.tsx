import React, { FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { PermissionActions, PermissionSubjects, PermissionType } from "@shared/Interfaces/permission.interface";
import { AppState } from "@shared/Redux/store";
import { permissionLevelCompare } from "@shared/Utils/PermissionHelper";

interface Props {
  subject: PermissionSubjects,
  action?: PermissionActions,
  type?: PermissionType,
}

const Guard: FunctionComponent<Props> = ({ children, action, subject, type }) => {
  const permissions = useSelector((state: AppState) => state.localUser.permissions);

  const allow = permissions.find(perm =>
    perm.subject === subject
    && (!action || perm.action === action)
    && (!type || permissionLevelCompare(perm.type, type)));

  if(!allow) return null;

  return (
    <>
      {children}
    </>
  );
};

export default Guard;
