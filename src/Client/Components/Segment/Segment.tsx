import React, { FunctionComponent, useState } from "react";
import "./Segment.scss";

interface Tab {
  name: string,
  child: JSX.Element,
}

interface Props {
  tabs: Tab[],
}

const Segment: FunctionComponent<Props> = ({ tabs }) => {
  const [selectedTab, setSelectedTab] = useState<Tab>(tabs[0]);

  const selectTab = (tab: string) => {
    const selected = tabs.find(t => t.name === tab);
    setSelectedTab(selected);
  };

  return (
    <div className="Segment">
      <div className="menu">
        {tabs.map(tab => <div key={tab.name} className="menuItem" onClick={() => selectTab(tab.name)}> {tab.name} </div>)}
      </div>
      <div className="content">
        {selectedTab && selectedTab.child}
      </div>
    </div>
  );
};

export default Segment;
