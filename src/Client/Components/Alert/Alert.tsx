import React from "react";
import classnames from "classnames";
import { AlertProps } from "./AlertProps.interface";
import "./Alert.scss";

const Alert = (props: AlertProps) => {
  const className = classnames(
    "Alert",
    `Alert--${props.type}`,
    props.className,
  );
  return (
    <div role="status" className={className}>
      {props.children}
    </div>
  );
};

export default Alert;
