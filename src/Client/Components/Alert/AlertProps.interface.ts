import { ReactNode } from "react";

export type AlertType = "success" | "warning" | "error" | "info";

export interface AlertProps {
  children: ReactNode,
  type: AlertType,
  className?: string,
}
