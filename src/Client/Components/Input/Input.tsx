import React from "react";
import { InputProps } from "./Input.interface";
import "./Input.scss";

const Input = React.forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  const { label, error, secondary, ...rest } = props;

  const className = `Input ${secondary ? "secondary" : ""}`;

  return (
    <div className={className}>
      { label && <label> {label} </label> }
      <input {...rest} ref={ref} />
      <div className={`error ${error ? "" : "hidden"}`}> {error} </div>
    </div>
  );
});

export default Input;
