import React, { FunctionComponent } from "react";
import { Button } from "..";
import "./Modal.scss";

interface Props {
  text: string,
  title: string,
  open: boolean,
  onClose: () => void,
  action: () => void,
}

const Modal: FunctionComponent<Props> = ({ text, title, open, onClose, action }) => {
  const className = `Modal ${open ? "open" : "close"}`;
  return (
    <div className={className}>
      <div className="modal">
        <div className="header">{title}</div>
        <div className="content">
          {text}
        </div>
        <div className="actions">
          <Button content="OK" onClick={action} />
          <Button content="CANCEL" onClick={onClose} />
        </div>
      </div>
    </div>
  );
};

export default Modal;
